/*
Navicat MySQL Data Transfer

Source Server         : Server Local
Source Server Version : 50532
Source Host           : 127.0.0.1:3306
Source Database       : db_rsigondanglegi

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2018-01-26 09:06:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `admin_nama` varchar(50) DEFAULT NULL,
  `admin_username` varchar(100) DEFAULT NULL,
  `admin_password` varchar(32) DEFAULT NULL,
  `admin_view_password` varchar(32) DEFAULT NULL,
  `admin_level` char(2) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin', 'admin', 'e00cf25ad42683b3df678c61f42c6bda', 'admin1', '1');

-- ----------------------------
-- Table structure for artikel_kesehatan
-- ----------------------------
DROP TABLE IF EXISTS `artikel_kesehatan`;
CREATE TABLE `artikel_kesehatan` (
  `arkes_id` int(11) NOT NULL,
  `arkes_judul` varchar(50) DEFAULT NULL,
  `arkes_penulis` varchar(50) DEFAULT NULL,
  `arkes_tanggal` date DEFAULT NULL,
  `arkes_deskripsi` text,
  `arkes_gambar` varchar(200) DEFAULT NULL,
  `arkes_title_meta` varchar(250) DEFAULT NULL,
  `arkes_keyword_meta` varchar(250) DEFAULT NULL,
  `arkes_deskripsi_meta` text,
  PRIMARY KEY (`arkes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of artikel_kesehatan
-- ----------------------------
INSERT INTO `artikel_kesehatan` VALUES ('1', 'MITOS DAN FAKTA HIV', ' B Wills', '2018-01-23', 'Tidak semua orang dengan HIV positif akan berkembang menjadi AIDS karena dengan terapi...', 'hiv.png', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');

-- ----------------------------
-- Table structure for dokter
-- ----------------------------
DROP TABLE IF EXISTS `dokter`;
CREATE TABLE `dokter` (
  `dokter_id` int(11) NOT NULL AUTO_INCREMENT,
  `spesialis_id` int(11) DEFAULT NULL,
  `dokter_nama` varchar(35) DEFAULT NULL,
  `dokter_gambar` varchar(200) DEFAULT NULL,
  `dokter_title_meta` varchar(250) DEFAULT NULL,
  `dokter_keyword_meta` varchar(250) DEFAULT NULL,
  `dokter_deskripsi_meta` text,
  PRIMARY KEY (`dokter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dokter
-- ----------------------------
INSERT INTO `dokter` VALUES ('1', '1', 'Edwin', 'edwin.jpg', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `dokter` VALUES ('2', '1', 'Njajal', 'edwin.jpg', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');

-- ----------------------------
-- Table structure for fasilitas_umum
-- ----------------------------
DROP TABLE IF EXISTS `fasilitas_umum`;
CREATE TABLE `fasilitas_umum` (
  `fasilitas_id` int(11) NOT NULL AUTO_INCREMENT,
  `fasilitas_nama` varchar(50) DEFAULT NULL,
  `fasilitas_deskripsi` text,
  `fasilitas_title_meta` varchar(250) DEFAULT NULL,
  `fasilitas_keyword_meta` varchar(250) DEFAULT NULL,
  `fasilitas_deskripsi_meta` text,
  PRIMARY KEY (`fasilitas_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of fasilitas_umum
-- ----------------------------
INSERT INTO `fasilitas_umum` VALUES ('1', 'Fasilitas Umum', '<table>\r\n<tr>\r\n    <td valign=\"top\" class=\"txtcontent\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n      <tr>\r\n        <td width=\"6%\" align=\"left\" valign=\"top\" class=\"txtcontentbold\">1.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtcontentbold\">Rawat Inap</strong></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Jumlah : 235 tempat tidur</td>\r\n      </tr>\r\n      <tr>\r\n        <td height=\"6\" align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">2.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">Rawat Jalan</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Dua puluh ruangan klinik yang didukung oleh lebih dari 50 bidang spesialis dan sub-spesialis.</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">3.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtjudul_content\">Pelayanan Instalasi Gawat Darurat 24 Jam (Telp. 031 - 5010555)</strong></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Didukung oleh Ambulan yang berisi peralatan   lengkap  dan obat emergensi. Instalasi Gawat Darurat dilengkapi dengan sarana   monitor pasien pada tiap tempat periksa, ruang pemeriksaaan, ruang   observasi, bahkan Kamar Tindakan/ Operasi untuk melakukan tindakan   sederhana.</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">4.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtjudul_content\">Pelayanan Perawatan Intensif / Intensive Care Service</strong></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">ICU (16 tempat tidur) <br />\r\n          Fasilitas Monitoring: Elektrokardiografi, saturasi oksigen, end tidal CO2, CVP, hemodinamik  (invasive dan non-invasive)</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">NICU (12 inkubator)<br />\r\n          Perawatan level I, II dan III - Fasilitas penunjang: Ventilator mekanik   tipe Avea Vyasis, Galileo dan Babylog     8000 plus; Light therapy     Dragger, Nasal CPAP Arabella, Echocardiografi tipe Philips, Mobile   X-ray, Syringe pump, Infusion Pump, Monitor, Portable Pulse Oxymetri,   Radiant infant warmer, Inkubator transport. </td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">5.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtjudul_content\">Kamar Operasi</strong></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n            <tr>\r\n              <td width=\"6%\" align=\"left\" valign=\"top\">-<br />\r\n                -<br /></td>\r\n              <td align=\"left\" valign=\"top\">10 teater<br />\r\n                OPMI Pentero Surgical Microscope, PHACO   Emulsification for Minimal Invasive Cataract Surgery, Endoscopy Unit for   Surgery and Diagnostic</td>\r\n            </tr>\r\n        </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">6.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtjudul_content\">Pusat Layanan Diagnostik</strong></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Radiology Diagnostic : </td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n            <tr>\r\n              <td width=\"6%\" align=\"left\" valign=\"top\">-<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -</td>\r\n              <td align=\"left\" valign=\"top\"> CT Scan 64 slices<br />\r\n                General And Cardiac Angiography<br />\r\n                MRI 1,5 TESLA<br />\r\n                Mammography<br />\r\n                Fluororadiography<br />\r\n                General Radiography<br />\r\n                Mobile X-Ray Unit<br />\r\n                Digital Dental Panoramic<br />\r\n                4D Ultrasonography<br />\r\n                Echocardiography<br />\r\n                EEG, EMG </td>\r\n            </tr>\r\n        </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Laboratory : <br />\r\n            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n              <tr>\r\n                <td width=\"6%\" align=\"left\" valign=\"top\">-<br />\r\n                  -<br />\r\n                  -<br /></td>\r\n                <td align=\"left\" valign=\"top\"> Anatomical Pathology <br />\r\n                  Clinical Pathology <br />\r\n                  Microbiology </td>\r\n              </tr>\r\n          </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Endoscopies : <br />\r\n            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n              <tr>\r\n                <td width=\"6%\" align=\"left\" valign=\"top\">-<br /></td>\r\n                <td align=\"left\" valign=\"top\">Diagnostic </td>\r\n              </tr>\r\n          </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Catheterization Laboratory : <br />\r\n            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n              <tr>\r\n                <td width=\"6%\" align=\"left\" valign=\"top\">-<br />\r\n                  -<br />\r\n                  -<br />\r\n                  -<br /></td>\r\n                <td align=\"left\" valign=\"top\">Pacemaker Implant <br />\r\n                  Diagnostic and Intervention for Coronary Disease <br />\r\n                  Diagnostic and Intervention for Congenital Disease <br />\r\n                  Diagnostic and Intervention for Peripher Arterial / Vascular Disease <br /></td>\r\n              </tr>\r\n          </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Other Diagnostic Equipmen t: <br />\r\n            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n              <tr>\r\n                <td width=\"6%\" align=\"left\" valign=\"top\">-<br /></td>\r\n                <td align=\"left\" valign=\"top\">EEG, EMG, TTE, TEE, Treadmill, Doppler Cardiovascular, Holter Monitoring, Tilt Table Test <br /></td>\r\n              </tr>\r\n          </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">7.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">Farmasi</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">8.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">Hemodialisa</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">9.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtjudul_content\">Rehabilitasi Medik &amp; Fisioterapi</strong></td>\r\n      </tr>\r\n    </table></td>\r\n  </tr>\r\n</table>', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');

-- ----------------------------
-- Table structure for foto
-- ----------------------------
DROP TABLE IF EXISTS `foto`;
CREATE TABLE `foto` (
  `foto_id` int(11) NOT NULL AUTO_INCREMENT,
  `foto_nama` varchar(35) DEFAULT NULL,
  `foto_gambar` varchar(250) DEFAULT NULL,
  `foto_title_meta` varchar(250) DEFAULT NULL,
  `foto_keyword_meta` varchar(250) DEFAULT NULL,
  `foto_deskripsi_meta` text,
  PRIMARY KEY (`foto_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of foto
-- ----------------------------
INSERT INTO `foto` VALUES ('1', 'Foto', '1.jpg', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `foto` VALUES ('2', 'Foto', '2.jpg', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `foto` VALUES ('3', 'Foto', '3.jpg', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `foto` VALUES ('4', 'Foto', '4.jpg', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `foto` VALUES ('5', 'Foto', '5.jpg', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `foto` VALUES ('6', 'Foto', '6.jpg', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `foto` VALUES ('7', 'Foto', '7.jpg', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `foto` VALUES ('8', 'Foto', '8.jpg', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');

-- ----------------------------
-- Table structure for indikator_mutu
-- ----------------------------
DROP TABLE IF EXISTS `indikator_mutu`;
CREATE TABLE `indikator_mutu` (
  `indikator_id` int(11) NOT NULL AUTO_INCREMENT,
  `indikator_nama` varchar(300) DEFAULT NULL,
  `indikator_standar` varchar(100) DEFAULT NULL,
  `indikator_pencapaian` varchar(200) DEFAULT NULL,
  `indikator_tahun` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`indikator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of indikator_mutu
-- ----------------------------
INSERT INTO `indikator_mutu` VALUES ('1', 'Keterlambatan Emergency Respon Time Dokter > 5 menit', '-', '	0%', '2017');
INSERT INTO `indikator_mutu` VALUES ('2', 'Angka kejadian IADP (Infeksi Aliran Darah Primer)', '< 2%', '	0,02%', '2017');
INSERT INTO `indikator_mutu` VALUES ('3', 'Angka Kematian Ibu melahirkan karena eklampsia', '< 30%', '0%', '2017');
INSERT INTO `indikator_mutu` VALUES ('4', 'Angka Kematian Ibu melahirkan karena perdarahan', '< 1%', '0%', '2017');
INSERT INTO `indikator_mutu` VALUES ('5', 'Angka Kematian Ibu melahirkan karena sepsis', '< 0,2%', '	0%', '2017');

-- ----------------------------
-- Table structure for informasi_umum
-- ----------------------------
DROP TABLE IF EXISTS `informasi_umum`;
CREATE TABLE `informasi_umum` (
  `infoum_id` int(11) NOT NULL AUTO_INCREMENT,
  `infoum_judul` varchar(250) DEFAULT NULL,
  `infoum_deskripsi` text,
  `infoum_gambar` varchar(250) DEFAULT NULL,
  `infoum_tanggal` date DEFAULT NULL,
  `infoum_title_meta` varchar(250) DEFAULT NULL,
  `infoum_keyword_meta` varchar(250) DEFAULT NULL,
  `infoum_deskripsi_meta` text,
  PRIMARY KEY (`infoum_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of informasi_umum
-- ----------------------------
INSERT INTO `informasi_umum` VALUES ('3', 'Masih Suka Memakai Iodine Untuk Mengobati Luka? Ternyata Hal ini Salah Kaprah Lho', 'Apa yang anda lakukan jika mengalami luka gores akibat benda tajam atau jatuh.', 'iodin.jpg', '2018-01-23', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');

-- ----------------------------
-- Table structure for kontak
-- ----------------------------
DROP TABLE IF EXISTS `kontak`;
CREATE TABLE `kontak` (
  `kontak_id` int(11) NOT NULL AUTO_INCREMENT,
  `kontak_lat` varchar(100) DEFAULT NULL,
  `kontak_long` varchar(100) DEFAULT NULL,
  `kontak_judul` varchar(30) DEFAULT NULL,
  `kontak_alamat` text,
  `kontak_telp` varchar(15) DEFAULT NULL,
  `kontak_email` varchar(30) DEFAULT NULL,
  `kontak_wa` varchar(15) DEFAULT NULL,
  `kontak_facebook` varchar(30) DEFAULT NULL,
  `kontak_ig` varchar(30) DEFAULT NULL,
  `kontak_title_meta` varchar(250) DEFAULT NULL,
  `kontak_keyword_meta` varchar(250) DEFAULT NULL,
  `kontak_deskripsi_meta` text,
  PRIMARY KEY (`kontak_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kontak
-- ----------------------------
INSERT INTO `kontak` VALUES ('1', '-8.1703578', '112.6380851', 'Kontak Kami', 'Jl. Hayam Wuruk No.66, Gondanglegi Wetan, Gondanglegi, Malang, Jawa Timur 65174', '(0341) 879047', 'Rsi@gmail.com', '085879090978', 'Rsi GondangLegi', 'Rsi GondangLegi', 'Rsi Gondanglegi', 'Rsi Gondanglegi', 'Rsi Gondanglegi');

-- ----------------------------
-- Table structure for mainmenu
-- ----------------------------
DROP TABLE IF EXISTS `mainmenu`;
CREATE TABLE `mainmenu` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `idmenu` int(11) NOT NULL,
  `nama_menu` varchar(50) NOT NULL,
  `active_menu` varchar(50) NOT NULL,
  `icon_class` varchar(50) NOT NULL,
  `link_menu` varchar(50) NOT NULL,
  `menu_akses` varchar(12) NOT NULL,
  `entry_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`seq`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mainmenu
-- ----------------------------
INSERT INTO `mainmenu` VALUES ('1', '1', 'Dashboard', '', 'menu-icon fa fa-tachometer', 'admin', '', '2018-01-13 11:32:59', null);
INSERT INTO `mainmenu` VALUES ('8', '8', 'Administrator', '', 'menu-icon fa fa-user', '#', '', '2017-10-13 12:57:17', null);
INSERT INTO `mainmenu` VALUES ('2', '2', 'Slider', '', 'menu-icon fa fa-file-image-o', 'slider', '', '2018-01-13 11:33:05', null);
INSERT INTO `mainmenu` VALUES ('3', '3', 'About', '', 'menu-icon fa fa-newspaper-o', 'about', '', '2018-01-13 11:33:17', null);
INSERT INTO `mainmenu` VALUES ('4', '4', 'Produk', '', 'menu-icon fa fa-briefcase', 'produk', '', '2018-01-13 11:33:28', null);
INSERT INTO `mainmenu` VALUES ('5', '5', 'Gallery', '', 'menu-icon fa fa-file-image-o', 'gallery', '', '2018-01-13 11:33:33', null);
INSERT INTO `mainmenu` VALUES ('6', '6', 'Kontak', '', 'menu-icon fa fa-book', 'kontak', '', '2018-01-13 11:33:35', null);
INSERT INTO `mainmenu` VALUES ('7', '7', 'Video', '', 'menu-icon fa fa-film', 'video', '', '2018-01-13 11:33:39', null);

-- ----------------------------
-- Table structure for master_jadwal
-- ----------------------------
DROP TABLE IF EXISTS `master_jadwal`;
CREATE TABLE `master_jadwal` (
  `jadwal_id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal_hari` varchar(200) DEFAULT NULL,
  `jadwal_jam` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`jadwal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_jadwal
-- ----------------------------
INSERT INTO `master_jadwal` VALUES ('1', 'Senin', '9.00 - 22.00');
INSERT INTO `master_jadwal` VALUES ('2', 'Selasa', '9.00 - 22.00');
INSERT INTO `master_jadwal` VALUES ('3', 'Rabu', '9.00 - 20.00');
INSERT INTO `master_jadwal` VALUES ('4', 'Kamis', '9.00 - 21.00');
INSERT INTO `master_jadwal` VALUES ('5', 'Jumat', '9.00 - 19.00');
INSERT INTO `master_jadwal` VALUES ('6', 'Sabtu', '9.00 - 18.00');

-- ----------------------------
-- Table structure for master_spesialis
-- ----------------------------
DROP TABLE IF EXISTS `master_spesialis`;
CREATE TABLE `master_spesialis` (
  `spesialis_id` int(11) NOT NULL AUTO_INCREMENT,
  `spesialis_nama` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`spesialis_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_spesialis
-- ----------------------------
INSERT INTO `master_spesialis` VALUES ('1', 'Spesialis Organ Dalam');

-- ----------------------------
-- Table structure for pelayanan_medis
-- ----------------------------
DROP TABLE IF EXISTS `pelayanan_medis`;
CREATE TABLE `pelayanan_medis` (
  `pelayanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `pelayanan_judul` varchar(200) DEFAULT NULL,
  `pelayanan_deskripsi` text,
  `pelayanan_title_meta` varchar(250) DEFAULT NULL,
  `pelayanan_keyword_meta` varchar(250) DEFAULT NULL,
  `pelayanan_deksripsi_meta` text,
  PRIMARY KEY (`pelayanan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pelayanan_medis
-- ----------------------------
INSERT INTO `pelayanan_medis` VALUES ('1', 'Pelayanan Medis', '<table>\r\n<tr>\r\n    <td valign=\"top\" class=\"txtcontent\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n      <tr>\r\n        <td width=\"6%\" align=\"left\" valign=\"top\" class=\"txtcontentbold\">1.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtcontentbold\">Rawat Inap</strong></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Jumlah : 235 tempat tidur</td>\r\n      </tr>\r\n      <tr>\r\n        <td height=\"6\" align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">2.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">Rawat Jalan</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Dua puluh ruangan klinik yang didukung oleh lebih dari 50 bidang spesialis dan sub-spesialis.</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">3.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtjudul_content\">Pelayanan Instalasi Gawat Darurat 24 Jam (Telp. 031 - 5010555)</strong></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Didukung oleh Ambulan yang berisi peralatan   lengkap  dan obat emergensi. Instalasi Gawat Darurat dilengkapi dengan sarana   monitor pasien pada tiap tempat periksa, ruang pemeriksaaan, ruang   observasi, bahkan Kamar Tindakan/ Operasi untuk melakukan tindakan   sederhana.</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">4.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtjudul_content\">Pelayanan Perawatan Intensif / Intensive Care Service</strong></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">ICU (16 tempat tidur) <br />\r\n          Fasilitas Monitoring: Elektrokardiografi, saturasi oksigen, end tidal CO2, CVP, hemodinamik  (invasive dan non-invasive)</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">NICU (12 inkubator)<br />\r\n          Perawatan level I, II dan III - Fasilitas penunjang: Ventilator mekanik   tipe Avea Vyasis, Galileo dan Babylog     8000 plus; Light therapy     Dragger, Nasal CPAP Arabella, Echocardiografi tipe Philips, Mobile   X-ray, Syringe pump, Infusion Pump, Monitor, Portable Pulse Oxymetri,   Radiant infant warmer, Inkubator transport. </td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">5.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtjudul_content\">Kamar Operasi</strong></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n            <tr>\r\n              <td width=\"6%\" align=\"left\" valign=\"top\">-<br />\r\n                -<br /></td>\r\n              <td align=\"left\" valign=\"top\">10 teater<br />\r\n                OPMI Pentero Surgical Microscope, PHACO   Emulsification for Minimal Invasive Cataract Surgery, Endoscopy Unit for   Surgery and Diagnostic</td>\r\n            </tr>\r\n        </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">6.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtjudul_content\">Pusat Layanan Diagnostik</strong></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Radiology Diagnostic : </td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n            <tr>\r\n              <td width=\"6%\" align=\"left\" valign=\"top\">-<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -</td>\r\n              <td align=\"left\" valign=\"top\"> CT Scan 64 slices<br />\r\n                General And Cardiac Angiography<br />\r\n                MRI 1,5 TESLA<br />\r\n                Mammography<br />\r\n                Fluororadiography<br />\r\n                General Radiography<br />\r\n                Mobile X-Ray Unit<br />\r\n                Digital Dental Panoramic<br />\r\n                4D Ultrasonography<br />\r\n                Echocardiography<br />\r\n                EEG, EMG </td>\r\n            </tr>\r\n        </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Laboratory : <br />\r\n            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n              <tr>\r\n                <td width=\"6%\" align=\"left\" valign=\"top\">-<br />\r\n                  -<br />\r\n                  -<br /></td>\r\n                <td align=\"left\" valign=\"top\"> Anatomical Pathology <br />\r\n                  Clinical Pathology <br />\r\n                  Microbiology </td>\r\n              </tr>\r\n          </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Endoscopies : <br />\r\n            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n              <tr>\r\n                <td width=\"6%\" align=\"left\" valign=\"top\">-<br /></td>\r\n                <td align=\"left\" valign=\"top\">Diagnostic </td>\r\n              </tr>\r\n          </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Catheterization Laboratory : <br />\r\n            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n              <tr>\r\n                <td width=\"6%\" align=\"left\" valign=\"top\">-<br />\r\n                  -<br />\r\n                  -<br />\r\n                  -<br /></td>\r\n                <td align=\"left\" valign=\"top\">Pacemaker Implant <br />\r\n                  Diagnostic and Intervention for Coronary Disease <br />\r\n                  Diagnostic and Intervention for Congenital Disease <br />\r\n                  Diagnostic and Intervention for Peripher Arterial / Vascular Disease <br /></td>\r\n              </tr>\r\n          </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Other Diagnostic Equipmen t: <br />\r\n            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n              <tr>\r\n                <td width=\"6%\" align=\"left\" valign=\"top\">-<br /></td>\r\n                <td align=\"left\" valign=\"top\">EEG, EMG, TTE, TEE, Treadmill, Doppler Cardiovascular, Holter Monitoring, Tilt Table Test <br /></td>\r\n              </tr>\r\n          </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">7.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">Farmasi</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">8.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">Hemodialisa</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">9.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtjudul_content\">Rehabilitasi Medik &amp; Fisioterapi</strong></td>\r\n      </tr>\r\n    </table></td>\r\n  </tr>\r\n</table>', 'Rsi GondangLegi', 'Rsi GondangLegi', 'Rsi GondangLegi');

-- ----------------------------
-- Table structure for penjadwalan_dokter
-- ----------------------------
DROP TABLE IF EXISTS `penjadwalan_dokter`;
CREATE TABLE `penjadwalan_dokter` (
  `pendok_id` int(11) NOT NULL AUTO_INCREMENT,
  `dokter_id` int(11) DEFAULT NULL,
  `jadwal_hari` varchar(200) DEFAULT NULL,
  `jadwal_jam` varchar(200) DEFAULT NULL,
  `jadwal_status` enum('N','Y') DEFAULT NULL,
  `pendok_title_meta` varchar(250) DEFAULT NULL,
  `pendok_keyword_meta` varchar(250) DEFAULT NULL,
  `pendok_deskripsi_meta` text,
  PRIMARY KEY (`pendok_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of penjadwalan_dokter
-- ----------------------------
INSERT INTO `penjadwalan_dokter` VALUES ('1', '1', 'Senin', '9.00 - 22.00', 'Y', 'RSI Gondanglegi', 'RSI Gondanglegi', 'RSI Gondanglegi');
INSERT INTO `penjadwalan_dokter` VALUES ('2', '1', 'Selasa', '9.00 - 20.00', 'Y', 'RSI Gondanglegi', 'RSI Gondanglegi', 'RSI Gondanglegi');
INSERT INTO `penjadwalan_dokter` VALUES ('3', '1', 'Rabu', '9.00 - 21.00', 'Y', 'RSI Gondanglegi', 'RSI Gondanglegi', 'RSI Gondanglegi');
INSERT INTO `penjadwalan_dokter` VALUES ('4', '1', 'Kamis', '9.00 - 21.00', 'Y', 'RSI Gondanglegi', 'RSI Gondanglegi', 'RSI Gondanglegi');
INSERT INTO `penjadwalan_dokter` VALUES ('5', '1', 'Jumat', '9.00 - 19.00', 'Y', 'RSI Gondanglegi', 'RSI Gondanglegi', 'RSI Gondanglegi');
INSERT INTO `penjadwalan_dokter` VALUES ('6', '1', 'Sabtu', '9.00 - 18.00', 'Y', 'RSI Gondanglegi', 'RSI Gondanglegi', 'RSI Gondanglegi');
INSERT INTO `penjadwalan_dokter` VALUES ('8', '2', 'Senin', '9.00 - 22.00', 'Y', 'RSI Gondanglegi', 'RSI Gondanglegi', 'RSI Gondanglegi');
INSERT INTO `penjadwalan_dokter` VALUES ('9', '2', 'Selasa', '9.00 - 20.00', 'Y', 'RSI Gondanglegi', 'RSI Gondanglegi', 'RSI Gondanglegi');
INSERT INTO `penjadwalan_dokter` VALUES ('10', '2', 'Rabu', '9.00 - 21.00', 'Y', 'RSI Gondanglegi', 'RSI Gondanglegi', 'RSI Gondanglegi');
INSERT INTO `penjadwalan_dokter` VALUES ('11', '2', 'Kamis', '9.00 - 21.00', 'Y', 'RSI Gondanglegi', 'RSI Gondanglegi', 'RSI Gondanglegi');
INSERT INTO `penjadwalan_dokter` VALUES ('12', '2', 'Jumat', '9.00 - 19.00', 'Y', 'RSI Gondanglegi', 'RSI Gondanglegi', 'RSI Gondanglegi');
INSERT INTO `penjadwalan_dokter` VALUES ('13', '2', 'Sabtu', '9.00 - 18.00', 'Y', 'RSI Gondanglegi', 'RSI Gondanglegi', 'RSI Gondanglegi');

-- ----------------------------
-- Table structure for penunjang_medis
-- ----------------------------
DROP TABLE IF EXISTS `penunjang_medis`;
CREATE TABLE `penunjang_medis` (
  `penunjang_id` int(11) NOT NULL AUTO_INCREMENT,
  `penunjang_judul` varchar(35) DEFAULT NULL,
  `penunjang_deskripsi` text,
  `penunjang_title_meta` varchar(250) DEFAULT NULL,
  `penunjang_keyword_meta` varchar(250) DEFAULT NULL,
  `penunjang_deksripsi_meta` text,
  PRIMARY KEY (`penunjang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of penunjang_medis
-- ----------------------------
INSERT INTO `penunjang_medis` VALUES ('1', 'Penunjang Medis', '<table>\r\n<tr>\r\n    <td valign=\"top\" class=\"txtcontent\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n      <tr>\r\n        <td width=\"6%\" align=\"left\" valign=\"top\" class=\"txtcontentbold\">1.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtcontentbold\">Rawat Inap</strong></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Jumlah : 235 tempat tidur</td>\r\n      </tr>\r\n      <tr>\r\n        <td height=\"6\" align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">2.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">Rawat Jalan</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Dua puluh ruangan klinik yang didukung oleh lebih dari 50 bidang spesialis dan sub-spesialis.</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">3.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtjudul_content\">Pelayanan Instalasi Gawat Darurat 24 Jam (Telp. 031 - 5010555)</strong></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Didukung oleh Ambulan yang berisi peralatan   lengkap  dan obat emergensi. Instalasi Gawat Darurat dilengkapi dengan sarana   monitor pasien pada tiap tempat periksa, ruang pemeriksaaan, ruang   observasi, bahkan Kamar Tindakan/ Operasi untuk melakukan tindakan   sederhana.</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">4.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtjudul_content\">Pelayanan Perawatan Intensif / Intensive Care Service</strong></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">ICU (16 tempat tidur) <br />\r\n          Fasilitas Monitoring: Elektrokardiografi, saturasi oksigen, end tidal CO2, CVP, hemodinamik  (invasive dan non-invasive)</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">NICU (12 inkubator)<br />\r\n          Perawatan level I, II dan III - Fasilitas penunjang: Ventilator mekanik   tipe Avea Vyasis, Galileo dan Babylog     8000 plus; Light therapy     Dragger, Nasal CPAP Arabella, Echocardiografi tipe Philips, Mobile   X-ray, Syringe pump, Infusion Pump, Monitor, Portable Pulse Oxymetri,   Radiant infant warmer, Inkubator transport. </td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">5.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtjudul_content\">Kamar Operasi</strong></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n            <tr>\r\n              <td width=\"6%\" align=\"left\" valign=\"top\">-<br />\r\n                -<br /></td>\r\n              <td align=\"left\" valign=\"top\">10 teater<br />\r\n                OPMI Pentero Surgical Microscope, PHACO   Emulsification for Minimal Invasive Cataract Surgery, Endoscopy Unit for   Surgery and Diagnostic</td>\r\n            </tr>\r\n        </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">6.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtjudul_content\">Pusat Layanan Diagnostik</strong></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Radiology Diagnostic : </td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n            <tr>\r\n              <td width=\"6%\" align=\"left\" valign=\"top\">-<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -<br />\r\n                -</td>\r\n              <td align=\"left\" valign=\"top\"> CT Scan 64 slices<br />\r\n                General And Cardiac Angiography<br />\r\n                MRI 1,5 TESLA<br />\r\n                Mammography<br />\r\n                Fluororadiography<br />\r\n                General Radiography<br />\r\n                Mobile X-Ray Unit<br />\r\n                Digital Dental Panoramic<br />\r\n                4D Ultrasonography<br />\r\n                Echocardiography<br />\r\n                EEG, EMG </td>\r\n            </tr>\r\n        </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Laboratory : <br />\r\n            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n              <tr>\r\n                <td width=\"6%\" align=\"left\" valign=\"top\">-<br />\r\n                  -<br />\r\n                  -<br /></td>\r\n                <td align=\"left\" valign=\"top\"> Anatomical Pathology <br />\r\n                  Clinical Pathology <br />\r\n                  Microbiology </td>\r\n              </tr>\r\n          </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Endoscopies : <br />\r\n            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n              <tr>\r\n                <td width=\"6%\" align=\"left\" valign=\"top\">-<br /></td>\r\n                <td align=\"left\" valign=\"top\">Diagnostic </td>\r\n              </tr>\r\n          </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Catheterization Laboratory : <br />\r\n            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n              <tr>\r\n                <td width=\"6%\" align=\"left\" valign=\"top\">-<br />\r\n                  -<br />\r\n                  -<br />\r\n                  -<br /></td>\r\n                <td align=\"left\" valign=\"top\">Pacemaker Implant <br />\r\n                  Diagnostic and Intervention for Coronary Disease <br />\r\n                  Diagnostic and Intervention for Congenital Disease <br />\r\n                  Diagnostic and Intervention for Peripher Arterial / Vascular Disease <br /></td>\r\n              </tr>\r\n          </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">&nbsp;</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\">Other Diagnostic Equipmen t: <br />\r\n            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n              <tr>\r\n                <td width=\"6%\" align=\"left\" valign=\"top\">-<br /></td>\r\n                <td align=\"left\" valign=\"top\">EEG, EMG, TTE, TEE, Treadmill, Doppler Cardiovascular, Holter Monitoring, Tilt Table Test <br /></td>\r\n              </tr>\r\n          </table></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">7.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">Farmasi</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">8.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">Hemodialisa</td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\" height=\"6\"></td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontent\"></td>\r\n      </tr>\r\n      <tr>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\">9.</td>\r\n        <td align=\"left\" valign=\"top\" class=\"txtcontentbold\"><strong class=\"txtjudul_content\">Rehabilitasi Medik &amp; Fisioterapi</strong></td>\r\n      </tr>\r\n    </table></td>\r\n  </tr>\r\n</table>', 'Rsi GondangLegi', 'Rsi GondangLegi', 'Rsi GondangLegi');

-- ----------------------------
-- Table structure for rekanan
-- ----------------------------
DROP TABLE IF EXISTS `rekanan`;
CREATE TABLE `rekanan` (
  `rekanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `rekanan_judul` varchar(35) DEFAULT NULL,
  `rekanan_gambar` varchar(250) DEFAULT NULL,
  `rekanan_kode` char(2) DEFAULT NULL,
  `rekanan_title_meta` varchar(250) DEFAULT NULL,
  `rekanan_keyword_meta` varchar(250) DEFAULT NULL,
  `rekanan_deskripsi_meta` text,
  PRIMARY KEY (`rekanan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rekanan
-- ----------------------------
INSERT INTO `rekanan` VALUES ('1', 'Asuransi Jiwa Tugu', 'Asuransi-Jiwa-Tugu.png', '1', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `rekanan` VALUES ('2', 'Asuransi Jiwasraya', 'Asuransi-Jiwasyara.png', '1', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `rekanan` VALUES ('3', 'Asuransi Mitra', 'Asuransi-Mitra.png', '1', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `rekanan` VALUES ('4', 'Asuransi MAG', 'Asuransi-Multi.png', '1', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `rekanan` VALUES ('5', 'Askes Ramayana', 'Asuransi-Ramayana.png', '1', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `rekanan` VALUES ('6', 'Asuransi Gesa', 'Gesa.png', '1', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `rekanan` VALUES ('7', 'Asuransi Sinarmas', 'Asuransi-Sinarmas.png', '1', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `rekanan` VALUES ('8', 'Asuransi Surya', 'Bapel-Surya.png', '2', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `rekanan` VALUES ('9', 'Baudarma Skin Klinik', 'Beauderma-Intiklinik.png', '2', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `rekanan` VALUES ('10', 'Belaputra Intiland', 'Belaputera-Intiland.png', '2', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `rekanan` VALUES ('11', 'Bhayangkara', 'Bhayangkara.png', '2', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `rekanan` VALUES ('12', 'Biofarma', 'Biofarma.png', '2', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `rekanan` VALUES ('13', 'Bloe-Dot', 'Bloe-Dot.png', '2', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `rekanan` VALUES ('14', 'Bpjs Kesehetan', 'BPJS-Kesehatan.png', '2', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');

-- ----------------------------
-- Table structure for sejarah
-- ----------------------------
DROP TABLE IF EXISTS `sejarah`;
CREATE TABLE `sejarah` (
  `sejarah_id` int(11) NOT NULL AUTO_INCREMENT,
  `sejarah_judul` varchar(50) DEFAULT NULL,
  `sejarah_deskripsi` text,
  `sejarah_gambar` varchar(200) DEFAULT NULL,
  `sejarah_title_meta` varchar(300) DEFAULT NULL,
  `sejarah_keyword_meta` varchar(300) DEFAULT NULL,
  `sejarah_deskripsi_meta` text,
  PRIMARY KEY (`sejarah_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sejarah
-- ----------------------------
INSERT INTO `sejarah` VALUES ('1', 'Sejarah Rumah Sakit Islam Gondanglegi', 'Sejarah singkat rumah sakit Islam gondanglegi malang. Pada saat itu Rakyat indonesia merdeka pada 17 agustus 1945 saat itu tengah menyambut hari kemerdekaan yang ke-8. Tak terkecualidi sebuah desa kecil di Malang selatan. Desa tersebut adalah kecamatan Gondanglegi Kabupaten Malang Propinsi Jawa Timur, dilihat dari kacamata geografis, Kecamatan Gondanglegi terletak di wilayah selatan Kabupaten Malang dengan ketinggian daerah kurang lebih 400m diatas permukaan laut. Pertama kali berdiri tahun 1953 masih di beri nama PETERMAS dan pada tahun 1981 berubah menjadi Muslimat NU setelah itu dilanjutkan menjadi BKIA Dewi Masyitoh dan pada tahun 1985 di rubah menjadi sebuah yayasan kesejahteraan islam (YKI). Selanjutnya dengan berjalan nya waktu akhirnya sampai akhirnya menjadi rumah sakit Islam gondang legi sampai sekarang', 'layanan.jpg', null, null, null);

-- ----------------------------
-- Table structure for setup
-- ----------------------------
DROP TABLE IF EXISTS `setup`;
CREATE TABLE `setup` (
  `setup_id` int(11) NOT NULL AUTO_INCREMENT,
  `setup_param` varchar(200) DEFAULT NULL,
  `setup_isi` text,
  `setup_kode` char(3) DEFAULT NULL,
  PRIMARY KEY (`setup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of setup
-- ----------------------------
INSERT INTO `setup` VALUES ('1', 'LOGO_FOOTER', 'header_logo.png', '1');
INSERT INTO `setup` VALUES ('2', 'TITLE', 'RSI Gondanglegi', '1');
INSERT INTO `setup` VALUES ('3', 'DESKRIPSI', 'RSI Gondanglegi adalah Rumah sakit yangPertama kali berdiri tahun 1953 masih di beri nama PETERMAS dan pada tahun 1981 berubah menjadi Muslimat NU setelah itu dilanjutkan menjadi BKIA Dewi Masyitoh dan pada tahun 1985 di rubah menjadi sebuah yayasan kesejahteraan islam (YKI). Selanjutnya dengan berjalan nya waktu akhirnya sampai akhirnya menjadi rumah sakit Islam gondang legi sampai sekarang', '1');
INSERT INTO `setup` VALUES ('4', 'ASURANSI', 'Asuransi', '1');
INSERT INTO `setup` VALUES ('5', 'ASURANSI', 'Non Asuransi', '2');
INSERT INTO `setup` VALUES ('6', 'FACEBOOK', 'facebook.com', '1');
INSERT INTO `setup` VALUES ('7', 'TWITTER', 'twitter.com', '1');
INSERT INTO `setup` VALUES ('8', 'INSTAGRAM', 'instagram.com', '1');
INSERT INTO `setup` VALUES ('9', 'YOUTUBE', 'youtube.com', '1');
INSERT INTO `setup` VALUES ('10', 'SEKILAS', 'Selamat Datang Di RSI Gondanglegi', '1');
INSERT INTO `setup` VALUES ('11', 'TELP', '+98 9875 5968 54', '1');
INSERT INTO `setup` VALUES ('12', 'LOGO_HEADER', 'header_logo.png', '1');

-- ----------------------------
-- Table structure for slider
-- ----------------------------
DROP TABLE IF EXISTS `slider`;
CREATE TABLE `slider` (
  `slider_id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_judul` varchar(50) DEFAULT NULL,
  `slider_subjudul` varchar(100) DEFAULT NULL,
  `slider_deskripsi` text,
  `slider_gambar` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`slider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slider
-- ----------------------------
INSERT INTO `slider` VALUES ('1', 'Selamat Datang Di  RSI Gondanglegi', 'Pusat Perawatan Kesehatan Anda', 'Sistem Pelayanan Kesehatan eHospital dengan senang hati melayani Ector County dan 21 wilayah di Permian Basin selama lebih dari 35 tahun', 'h1.jpg');
INSERT INTO `slider` VALUES ('2', 'Selamat Datang Di  RSI Gondanglegi', 'Pusat Perawatan Kesehatan Anda', 'Sistem Pelayanan Kesehatan eHospital dengan senang hati melayani Ector County dan 21 wilayah di Permian Basin selama lebih dari 35 tahun', 'h1.jpg');

-- ----------------------------
-- Table structure for submenu
-- ----------------------------
DROP TABLE IF EXISTS `submenu`;
CREATE TABLE `submenu` (
  `id_sub` int(11) NOT NULL AUTO_INCREMENT,
  `nama_sub` varchar(50) NOT NULL,
  `mainmenu_idmenu` int(11) NOT NULL,
  `active_sub` varchar(20) NOT NULL,
  `icon_class` varchar(100) NOT NULL,
  `link_sub` varchar(50) NOT NULL,
  `sub_akses` varchar(12) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_user` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_sub`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of submenu
-- ----------------------------
INSERT INTO `submenu` VALUES ('1', 'Entry User', '8', '', '', 'User', '', '2017-10-17 12:28:25', null);
INSERT INTO `submenu` VALUES ('2', 'Kategori Produk', '4', '', '', 'Produk', '', '2017-10-17 12:34:17', null);
INSERT INTO `submenu` VALUES ('3', 'Produk', '4', '', '', 'Produk/detail', '', '2017-10-17 12:34:26', null);
INSERT INTO `submenu` VALUES ('4', 'Album', '5', '', '', 'Gallery', '', '2017-10-17 12:34:34', null);
INSERT INTO `submenu` VALUES ('5', 'Foto', '5', '', '', 'Gallery/foto', '', '2017-10-17 12:34:40', null);

-- ----------------------------
-- Table structure for tab_akses_mainmenu
-- ----------------------------
DROP TABLE IF EXISTS `tab_akses_mainmenu`;
CREATE TABLE `tab_akses_mainmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  `c` int(11) DEFAULT '0',
  `r` int(11) DEFAULT '0',
  `u` int(11) DEFAULT '0',
  `d` int(11) DEFAULT '0',
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_user` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tab_akses_mainmenu
-- ----------------------------
INSERT INTO `tab_akses_mainmenu` VALUES ('1', '1', '1', null, '1', null, null, '2017-09-25 11:49:01', 'direktur');
INSERT INTO `tab_akses_mainmenu` VALUES ('2', '8', '1', '0', '0', '0', '0', '2017-10-18 13:47:26', '');
INSERT INTO `tab_akses_mainmenu` VALUES ('3', '2', '1', '0', '1', '0', '0', '2017-10-13 14:29:46', '');
INSERT INTO `tab_akses_mainmenu` VALUES ('4', '3', '1', '0', '1', '0', '0', '2017-10-14 09:31:55', '');
INSERT INTO `tab_akses_mainmenu` VALUES ('5', '4', '1', '0', '1', '0', '0', '2017-10-15 14:22:01', '');
INSERT INTO `tab_akses_mainmenu` VALUES ('6', '5', '1', '0', '1', '0', '0', '2017-10-15 14:24:32', '');
INSERT INTO `tab_akses_mainmenu` VALUES ('7', '6', '1', '0', '1', '0', '0', '2017-10-18 13:47:15', '');
INSERT INTO `tab_akses_mainmenu` VALUES ('8', '7', '1', '0', '1', '0', '0', '2017-10-26 15:52:10', '');
INSERT INTO `tab_akses_mainmenu` VALUES ('23', '9', '0', '0', '1', '0', '0', '2018-01-26 09:02:19', '');

-- ----------------------------
-- Table structure for tab_akses_submenu
-- ----------------------------
DROP TABLE IF EXISTS `tab_akses_submenu`;
CREATE TABLE `tab_akses_submenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sub_menu` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  `c` int(11) DEFAULT '0',
  `r` int(11) DEFAULT '0',
  `u` int(11) DEFAULT '0',
  `d` int(11) DEFAULT '0',
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_user` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tab_akses_submenu
-- ----------------------------
INSERT INTO `tab_akses_submenu` VALUES ('1', '1', '1', '0', '1', '0', '0', '2017-10-13 12:45:40', '');
INSERT INTO `tab_akses_submenu` VALUES ('2', '2', '1', '0', '1', '0', '0', '2017-10-15 17:59:02', '');
INSERT INTO `tab_akses_submenu` VALUES ('3', '3', '1', '0', '0', '0', '0', '2017-10-17 23:12:32', '');
INSERT INTO `tab_akses_submenu` VALUES ('4', '4', '1', '0', '1', '0', '0', '2017-10-15 17:59:16', '');
INSERT INTO `tab_akses_submenu` VALUES ('5', '5', '1', '0', '0', '0', '0', '2017-10-17 23:12:33', '');

-- ----------------------------
-- Table structure for testimoni
-- ----------------------------
DROP TABLE IF EXISTS `testimoni`;
CREATE TABLE `testimoni` (
  `testimoni_id` int(11) NOT NULL AUTO_INCREMENT,
  `testimoni_nama` varchar(35) DEFAULT NULL,
  `testimoni_deskripsi` text,
  `testimoni_gambar` varchar(200) DEFAULT NULL,
  `testimoni_title_meta` varchar(250) DEFAULT NULL,
  `testimoni_keyword_meta` varchar(250) DEFAULT NULL,
  `testimoni_deskripsi_meta` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`testimoni_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of testimoni
-- ----------------------------
INSERT INTO `testimoni` VALUES ('1', 'Ana Andreea', ' Biasanya saya tidak menulis surat, namun saya merasa dibatasi untuk menulis yang ini. Hal ini mengacu pada kepedihan administrasi penderitaan yang terletak di lantai empat di Rumah Sakit Umum eHospital, Melbourne.', 's1.jpg', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `testimoni` VALUES ('2', 'Simone Andreea', 'Saya senang menjadi pasien rawat jalan di Unit Bedah Ambulatory Anda untuk operasi kaki. Dari saat saya berjalan-jalan di jam awal jam 6:00 pagi, sampai saya dibebaskan, saya mengalami staf yang ramah, hangat dan mahir yang memperlakukan saya dengan sangat hati-hati.', 's2.jpg', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `testimoni` VALUES ('3', 'Imron Andreea', 'Saya senang menjadi pasien rawat jalan di Unit Bedah Ambulatory Anda untuk operasi kaki. Dari saat saya berjalan-jalan di jam awal jam 6:00 pagi, sampai saya dibebaskan, saya mengalami staf yang ramah, hangat dan mahir yang memperlakukan saya dengan sangat hati-hati.', 's2.jpg', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `testimoni` VALUES ('4', 'Edwin Andreea', 'Saya senang menjadi pasien rawat jalan di Unit Bedah Ambulatory Anda untuk operasi kaki. Dari saat saya berjalan-jalan di jam awal jam 6:00 pagi, sampai saya dibebaskan, saya mengalami staf yang ramah, hangat dan mahir yang memperlakukan saya dengan sangat hati-hati.', 's2.jpg', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');

-- ----------------------------
-- Table structure for user_type
-- ----------------------------
DROP TABLE IF EXISTS `user_type`;
CREATE TABLE `user_type` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(200) NOT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_type
-- ----------------------------
INSERT INTO `user_type` VALUES ('1', 'Administrator');

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video` (
  `video_id` int(11) NOT NULL AUTO_INCREMENT,
  `video` varchar(250) DEFAULT NULL,
  `video_title_meta` varchar(250) DEFAULT NULL,
  `video_keyword_meta` varchar(250) DEFAULT NULL,
  `video_deskripsi_meta` text,
  PRIMARY KEY (`video_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of video
-- ----------------------------
INSERT INTO `video` VALUES ('1', 'KDC9fHPhGto', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');
INSERT INTO `video` VALUES ('2', 'KDC9fHPhGto', 'RSI GondangLegi', 'RSI GondangLegi', 'RSI GondangLegi');

-- ----------------------------
-- Table structure for visi_misi
-- ----------------------------
DROP TABLE IF EXISTS `visi_misi`;
CREATE TABLE `visi_misi` (
  `vismis_id` int(11) NOT NULL AUTO_INCREMENT,
  `vismis_judul` varchar(30) DEFAULT NULL,
  `vismis_deskripsi` text,
  `vismis_gambar` varchar(200) DEFAULT NULL,
  `vismis_title_meta` varchar(250) DEFAULT NULL,
  `vismis_keyword_meta` varchar(250) DEFAULT NULL,
  `vismis_deskripsi_meta` text,
  PRIMARY KEY (`vismis_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of visi_misi
-- ----------------------------
INSERT INTO `visi_misi` VALUES ('1', null, '<h2>Visi</h2>\r\n						<p class=\"ulockd-about-para\">\r\n							1. Meningkatkan mutu pelayanan yang Islami.<br />\r\n							2. Tersusunnya standart pelayanan rumah sakit.<br />\r\n							3. Meningkatkan mutu dan profesionalisme sumber daya manusia.<br />\r\n							4. Meningkatkan pendapatan rumah sakit dengan mengelolaan yang efisien dan efektif\r\n						</p>\r\n\r\n            <h2>Misi</h2>\r\n						<p class=\"ulockd-about-para-two\">\r\n							1. Memberikan pelayanan prima berdasarkan etika, disiplin profesi yang dijiwai nilai keislaman. <br/>\r\n							2. Mengembangkan profesionalisme sumber daya manusia melalui pendidikan, pelatihan dan penelitian.<br />\r\n							3. Meningkatkan pendapatan rumah sakit dan karyawannya.<br />\r\n							4. Mengembangkan jaringan kerja sama dan rumah sakit pendidikan regional dan internasional.\r\n						</p>', 'layanan.jpg', 'Rsi Gondanglegi', null, null);
