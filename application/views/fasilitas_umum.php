<div class="ulockd-inner-home">
		<div class="container text-center">
			<div class="row">
				<div class="ulockd-inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase">Fasilitas Umum</h1>
					</div>
					<div class="col-md-12">
						<div class="ulockd-icd-layer">
							<ul class="list-inline ulockd-icd-sub-menu">
								<li><a href="#"> Beranda </a></li>
								<li><a href="#"> > </a></li>
								<li> <a href="#"> Fasilitas Umum </a> </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>

<section class="ulockd-career">
		<div class="container">
			<div class="row"><br/>
				<div class="col-lg-6 col-lg-offset-3 text-center">
					<div class="ulockd-testimonial-title hvr-float-shadow">
						<div class="ulockd-title-icon"><span class="flaticon-medical-kit"></span></div>
						<h2 class="text-uppercase"><?php echo $fasilitas_umum->fasilitas_nama ;?></h2>

					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-lg-offset-3 text-left">
					<div class="ulockd-srvc-title hvr-float-shadow">
						<?php echo $fasilitas_umum->fasilitas_deskripsi ;?>
					</div>
				</div>
			</div>
		</div>
</section>
