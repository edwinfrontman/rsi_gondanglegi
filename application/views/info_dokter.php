<div class="ulockd-inner-home">
		<div class="container text-center">
			<div class="row">
				<div class="ulockd-inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase">Info Dokter</h1>
					</div>
					<div class="col-md-12">
						<div class="ulockd-icd-layer">
							<ul class="list-inline ulockd-icd-sub-menu">
								<li><a href="#"> Beranda </a></li>
								<li><a href="#"> > </a></li>
								<li> <a href="#"> Info Dokter </a> </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>

<section class="ulockd-team-one">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-lg-offset-3 text-center">
					<div class="ulockd-testimonial-title hvr-float-shadow">
						<div class="ulockd-title-icon" style="right: 100px;"><span class="flaticon-medical-kit"></span></div>
						<h2 class="text-uppercase" style="margin-right: 99px;">Info Dokter</h2>

					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="row">
								<div role="alert" class="alert alert-success"> <form>
									<div class="form-group">
										<div class="col-sm-4">
										<select name="spesialis" id="spesialis" class="form-control" onchange="getDokter()">
											<option>--Pilih Spesialis--</option>
											<?php foreach($spesialis as $row_kat)	{	?>
												<option value="<?php echo $row_kat['spesialis_id']?>"><?php echo $row_kat['spesialis_nama']?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-sm-4">
										<select name="dokter" id="dokter" class="form-control">
											<option>--Pilih Spesialis Dulu--</option>
										</select>
									</div>
									</div>



								</form></div>


					</div>
				</div>

			</div>
		</div>
</section>
<span id="kota"></span>

<script>
	var link = "<?php echo site_url('Info_Dokter')?>";

	$(function(){ // sama dengan $(document).ready(function(){

		$('#dokter').change(function(){

			$('#kota').html("<img style='display: block; margin: 0 auto; text-align: center;' src='<?php echo base_url();?>assets/images/spinner/loader-dark.gif'>"); //Menampilkan loading selama proses pengambilan data kota
			var id = $(this).val();


			$.get("<?php echo base_url('Info_Dokter/create_load'); ?>", {id:id}, function(data){
                $('#kota').html(data);
            });
      //setTimeout(function(){
              //$("#example1").dataTable();
      //}, 3000);
		});

	});

	function getDokter(){
        $.get(link+'/getDokter/'+$('#spesialis').val(), $(this).serialize())
        .done(function(data) {
          $('#dokter').html(data);
        });
    }

</script>
