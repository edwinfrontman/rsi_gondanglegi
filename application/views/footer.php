<!-- Our Footer -->
<section class="ulockd-footer">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
        <div class="ulockd-footer-lnews">
            <img alt="" src="<?php echo base_url().'assets/images/'.$logo->setup_isi; ?>" class="img-responsive ulockd-footer-log">
            <h3 class="text-uppercase"><?php echo $title->setup_isi;?></h3>
            <p class="ulockd-ftr-text">
              <?php echo $deskripsi->setup_isi;?>
            </p>
      </div>

      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
        <div class="ulockd-footer-lnews">
          <h3 class="text-uppercase">Artikel Terbaru</h3>
          <?php foreach($artikel as $artikel_baru):
            $deskripsi = $artikel_baru['arkes_deskripsi'];
          	$cut=substr($deskripsi,0,150);
          ?>

          <div class="ulockd-media-box">
            <div class="media">
              <div class="media-left pull-left">
                <a class="thumbnail" href="#">
                  <img class="media-object" src="<?php echo base_url().'assets/images/'.$artikel_baru['arkes_gambar']; ?>" alt="s1.jpg">
                </a>
              </div>
              <div class="media-body">
              <a href="#" class="post-date"><?= longdate_indo($artikel_baru['arkes_tanggal']);?></a>
                <h4 class="media-heading"><?php echo $artikel_baru['arkes_judul']?></h4>
                <p><?php echo $cut;?><a style="color:#FFF" class="ulockd-bp-btn" href="<?php echo base_url('Artikel_kesehatan/detail_artikel/'.$artikel_baru['arkes_id']); ?>"> Baca Selengkapnya...</a></p>
              </div>
            </div>
          </div>
          <?php endforeach?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
        <div class="ulockd-footer-lnews">
          <h3 class="text-uppercase">Kontak Kami</h3>
          <p class="ulockd-ftr-text">
            <p style="color:#FFF; font-size:18px;"><i class='fa fa-map-marker'></i><span>&nbsp;<?php echo $kontak->kontak_alamat;?></span></p>
            <p style="color:#FFF; font-size:18px;"><i class='fa fa-envelope-open-o'></i><span>&nbsp;<?php echo $kontak->kontak_email;?></span></p>
            <p style="color:#FFF; font-size:18px;"><i class='fa fa-whatsapp'></i><span>&nbsp;<?php echo $kontak->kontak_wa;?></span></p>
            <p style="color:#FFF; font-size:18px;"><i class='fa fa-facebook-square'></i><span>&nbsp;<?php echo $kontak->kontak_facebook;?></span></p>
            <p style="color:#FFF; font-size:18px;"><i class='fa fa-instagram'></i><span>&nbsp;<?php echo $kontak->kontak_ig;?></span></p>
            <p style="color:#FFF; font-size:18px;"><i class='fa fa-address-book'></i><span>&nbsp;<?php echo $kontak->kontak_telp;?></span></p>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Our CopyRight Section -->
<section class="ulockd-copy-right">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p>Copyright© eHospital 2017.Unlock Design All right reserved.</p>
      </div>
    </div>
  </div>
</section>

<a class="scrollToHome" href="#"><i class="fa fa-home"></i></a>
</div>
<!-- Wrapper End -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootsnav.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/parallax.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/scrollto.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-scrolltofixed-min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/gallery.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/slider.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/video-player.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.barfiller.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/timepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/tweetie.js"></script>
<!-- Custom script for all pages -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>

</body>

<!-- Mirrored from unlockdesizn.com/html/health-and-beauty/eHospital/demo/index-multipage.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Jan 2018 02:36:28 GMT -->
</html>
