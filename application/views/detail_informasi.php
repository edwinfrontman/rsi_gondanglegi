<div class="ulockd-inner-home">
		<div class="container text-center">
			<div class="row">
				<div class="ulockd-inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase"><?php echo $detail_informasi->infoum_judul;?></h1>
					</div>
					<div class="col-md-12">
						<div class="ulockd-icd-layer">
							<ul class="list-inline ulockd-icd-sub-menu">
								<li><a href="#"> Beranda </a></li>
								<li><a href="#"> > </a></li>
								<li> <a href="#">Detail Informasi Kesehatan </a> </li>
								<li><a href="#"> > </a></li>
								<li> <a href="#"><?php echo $detail_informasi->infoum_judul;?></a> </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>


<section class="ulockd-service-details">
		<div class="container">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12 ulockd-mrgn1210">
						<div class="ulockd-project-sm-thumb">
							<img class="img-responsive img-whp" src="<?php echo base_url();?>assets/images/<?php echo $detail_informasi->infoum_gambar;?>" alt="">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 ulockd-mrgn1210">
						<div class="ulockd-pd-content">
							<div class="ulockd-bp-date">
								<ul class="list-inline">
									<li class="ulockd-bp-date-innner"> <a href="#"><h4><?= longdate_indo($detail_informasi->infoum_tanggal);?></h4></a></li>

								</ul>
							</div>
							<h3><?php echo $detail_informasi->infoum_judul;?></h3>
							<p class="project-dp-one"><?php echo $detail_informasi->infoum_deskripsi;?></p>

						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
