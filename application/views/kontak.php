<div class="ulockd-inner-home">
		<div class="container text-center">
			<div class="row">
				<div class="ulockd-inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase">Kontak Kami</h1>
					</div>
					<div class="col-md-12">
						<div class="ulockd-icd-layer">
							<ul class="list-inline ulockd-icd-sub-menu">
								<li><a href="#"> Beranda </a></li>
								<li><a href="#"> > </a></li>
								<li> <a href="#"> Kontak Kami </a> </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>

<section class="ulockd-about-two stellar">
		<div class="container">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<div class="ulockd-testimonial-title hvr-float-shadow">
					<div class="ulockd-title-icon"><span class="flaticon-medical-kit"></span></div>
					<h2 class="text-uppercase">KOntak Kami</h2>

				</div>
			</div>
			<div class="row">
				<div class="col-md-7">
					<div class="ulockd-abouttwo-details">
						<h2><?php echo $kontak->kontak_judul; ?></h2>
						<ul class=" ulockd-footer-font-icon">
							<li style="font-size:18px;"><a href="#"><i class="fa fa-map-marker"></i></a> &nbsp;<?php echo $kontak->kontak_alamat;?></li><br/>
							<li style="font-size:18px;"><a href="#"><i class="fa fa-envelope-open-o"></i></a> &nbsp;<?php echo $kontak->kontak_email;?></li><br/>
							<li style="font-size:18px;"><a href="#"><i class="fa fa-whatsapp"></i></a> &nbsp;<?php echo $kontak->kontak_wa;?></li><br/>
							<li style="font-size:18px;"><a href="#"><i class="fa fa-facebook-square"></i></a> &nbsp;<?php echo $kontak->kontak_facebook;?></li><br/>
							<li style="font-size:18px;"><a href="#"><i class="fa fa-instagram"></i></a> &nbsp;<?php echo $kontak->kontak_ig;?></li><br/>
							<li style="font-size:18px;"><a href="#"><i class="fa fa-address-book"></i></a> &nbsp;<?php echo $kontak->kontak_telp;?></li><br/>
						</ul>
					</div>
				</div>
				<div class="col-md-5">
					<div class="ulockd-fector-about-video">
						<div id="peta" style="width:100%;height:500px;"></div>
					</div>
				</div>
			</div>

		</div>
</section>
<script     src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFTimIhQoFCg8bF7PAMgDWi38QqqvaCx8">
  </script>
<script type="text/javascript">
		(function() {
		window.onload = function() {
		var map;
		//Parameter Google maps
		var options = {
		zoom: 16, //level zoom
		//posisi tengah peta
		center:new google.maps.LatLng('<?php echo @$kontak->kontak_lat;?>' ,'<?php echo @$kontak->kontak_long?>'),
		mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		// Buat peta di
		var map = new google.maps.Map(document.getElementById('peta'), options);
		// Tambahkan Marker
		var locations = [
			['<?php echo @$wisata->wisata_nama?>', '<?php echo @$kontak->kontak_lat;?>' ,'<?php echo @$kontak->kontak_long?>'],
		];
		var infowindow = new google.maps.InfoWindow();

		var marker, i;
		/* kode untuk menampilkan banyak marker */
		for (i = 0; i < locations.length; i++) {
			marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			map: map,
		});
		/* menambahkan event clik untuk menampikan
		infowindows dengan isi sesuai denga
		marker yang di klik */

		google.maps.event.addListener(marker, 'click', (function(marker, i) {
		return function() {
		infowindow.setContent(locations[i][0]);
		infowindow.open(map, marker);
		}
		})(marker, i));
		}


		};
		})();



	</script>
