<div class="ulockd-inner-home">
		<div class="container text-center">
			<div class="row">
				<div class="ulockd-inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase">Artikel Kesehatan</h1>
					</div>
					<div class="col-md-12">
						<div class="ulockd-icd-layer">
							<ul class="list-inline ulockd-icd-sub-menu">
								<li><a href="#"> Beranda </a></li>
								<li><a href="#"> > </a></li>
								<li> <a href="#"> Artikel Kesehatan </a> </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>

<section class="ulockd-ip-latest-news">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-lg-offset-3 text-center">
					<div class="ulockd-testimonial-title hvr-float-shadow">
						<div class="ulockd-title-icon" style="right: 100px;"><span class="flaticon-medical-kit"></span></div>
						<h2 class="text-uppercase" style="margin-right: 99px;">Artikel Kesehatan</h2>

					</div>
				</div>
			</div>
			<div class="row">
				<span id="results"></span>

			</div>
			<div align="center"><a href = "javascript:void(0)" onclick = "load_more()"><button type="button" class="btn btn-danger" style="width: 323px;height: 49px;">Lihat Data Lainnya..</button></a></div>
		</div>
</section>

<script type='text/javascript' language='javascript'>
		var total_record = 0;
		var total_groups = <?php echo $total_data;?>;
		$(document).ready(function(){

			$('#results').load("<?php echo site_url('Artikel_kesehatan/load_artikel') ?>", {'offset':total_record}, function() {total_record++;});

		});

		function load_more(){
			if(total_record <= total_groups)
        {
            loading = true;
            $('.loader_image').show();
            $.post('<?php echo site_url('Artikel_kesehatan/load_artikel') ?>',{'offset': total_record},
            function(data){
                if (data != "") {
                    $("#results").append(data);
                    $('.loader_image').hide();
                    total_record++;
                }
            });
        }
		}
</script>
