<div class="ulockd-inner-home">
		<div class="container text-center">
			<div class="row">
				<div class="ulockd-inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase">Non-Asuransi</h1>
					</div>
					<div class="col-md-12">
						<div class="ulockd-icd-layer">
							<ul class="list-inline ulockd-icd-sub-menu">
								<li><a href="#"> Beranda </a></li>
								<li><a href="#"> > </a></li>
								<li> <a href="#"> Non-Asuransi </a> </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
</div>
</div>
<section class="ulockd-partner">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-lg-offset-3 text-center">
					<div class="ulockd-team-title hvr-float-shadow">
						<h2 class="text-uppercase">Non-Asuransi</h2>

					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2 col-sm-4 col-xs-6"><div class="ulockd-partner-thumb text-center"><img src="http://rsborromeus.com/v2/wp-content/uploads/2017/02/Bapel-Surya.png" alt=""></div></div>
				<div class="col-md-2 col-sm-4 col-xs-6"><div class="ulockd-partner-thumb text-center"><img src="http://rsborromeus.com/v2/wp-content/uploads/2017/02/Beauderma-Intiklinik.png" alt=""></div></div>
				<div class="col-md-2 col-sm-4 col-xs-6"><div class="ulockd-partner-thumb text-center"><img src="http://rsborromeus.com/v2/wp-content/uploads/2017/02/Belaputera-Intiland.png" alt=""></div></div>
				<div class="col-md-2 col-sm-4 col-xs-6"><div class="ulockd-partner-thumb text-center"><img src="http://rsborromeus.com/v2/wp-content/uploads/2017/02/Bhayangkara.png" alt=""></div></div>
				<div class="col-md-2 col-sm-4 col-xs-6"><div class="ulockd-partner-thumb text-center"><img src="http://rsborromeus.com/v2/wp-content/uploads/2017/02/Biofarma.png" alt=""></div></div>
				<div class="col-md-2 col-sm-4 col-xs-6"><div class="ulockd-partner-thumb text-center"><img src="http://rsborromeus.com/v2/wp-content/uploads/2017/02/Bloe-Dot.png" alt=""></div></div>
				<div class="col-md-2 col-sm-4 col-xs-6"><div class="ulockd-partner-thumb text-center"><img src="http://rsborromeus.com/v2/wp-content/uploads/2017/02/BPJS-Kesehatan.png" alt=""></div></div>
				<div class="col-md-2 col-sm-4 col-xs-6"><div class="ulockd-partner-thumb text-center"><img src="http://rsborromeus.com/v2/wp-content/uploads/2017/02/Bukit-Asam.png" alt=""></div></div>
				<div class="col-md-2 col-sm-4 col-xs-6"><div class="ulockd-partner-thumb text-center"><img src="http://rsborromeus.com/v2/wp-content/uploads/2017/02/Citra-Karya-Pranata.png" alt=""></div></div>
				<div class="col-md-2 col-sm-4 col-xs-6"><div class="ulockd-partner-thumb text-center"><img src="http://rsborromeus.com/v2/wp-content/uploads/2017/02/Daya-Adicipta.png" alt=""></div></div>
			</div>
		</div>
</section>
