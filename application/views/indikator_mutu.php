<div class="ulockd-inner-home">
		<div class="container text-center">
			<div class="row">
				<div class="ulockd-inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase">Indikator Mutu</h1>
					</div>
					<div class="col-md-12">
						<div class="ulockd-icd-layer">
							<ul class="list-inline ulockd-icd-sub-menu">
								<li><a href="#"> Beranda </a></li>
								<li><a href="#"> > </a></li>
								<li> <a href="#"> Indikator Mutu </a> </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
</div>
</div>

<section class="ulockd-ap-faq">
		<div class="container">
        <div class="row">
                <div class="col-lg-6 col-lg-offset-3 text-center">
                    <div class="ulockd-testimonial-title hvr-float-shadow">
                        <div class="ulockd-title-icon" style="right: 100px;"><span class="flaticon-medical-kit"></span></div>
                        <h2 class="text-uppercase" style="margin-right: 99px;">Indikator Mutu</h2>

                    </div>
                </div>
        </div>
		<div class="row">
        
        <div class="col-sm-12">
        <table class="table"> 
        <thead> 
        <tr>
        <th>Indikator</th> 
        <th>Standar</th> 
        <th>Pencapaian</th> 
        </tr> 
        </thead> 
        <tbody> 
        <tr> 
        <?php
        foreach ($indikator as $row)
        {
            
        ?>
            <td><?php echo $row->indikator_nama;?></td> 
            <td><?php echo $row->indikator_standar;?></td> 
            <td><?php echo $row->indikator_pencapaian;?></td>
        
        </tr>  
        <?php }?> 
        </tbody>
        </table>
        </div>
    </div>
  </div>
</section>


