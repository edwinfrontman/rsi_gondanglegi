<div class="ulockd-inner-home">
		<div class="container text-center">
			<div class="row">
				<div class="ulockd-inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase">Rekanan</h1>
					</div>
					<div class="col-md-12">
						<div class="ulockd-icd-layer">
							<ul class="list-inline ulockd-icd-sub-menu">
								<li><a href="#"> Beranda </a></li>
								<li><a href="#"> > </a></li>
								<li> <a href="#"> Rekanan </a> </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
</div>
</div>
<section class="ulockd-partner">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-lg-offset-3 text-center">
					<div class="ulockd-testimonial-title hvr-float-shadow">
						<div class="ulockd-title-icon" style="right: 100px;"><span class="flaticon-medical-kit"></span></div>
						<h2 class="text-uppercase" style="margin-right: 99px;">Rekanan</h2>

					</div>
				</div>
			</div>
			<div class="row">
				<?php foreach($asuransi as $asuransie):?>
				<div class="col-md-2 col-sm-4 col-xs-6"><div class="ulockd-partner-thumb text-center"><img src="<?php echo base_url().'assets/images/'.$asuransie['rekanan_gambar']; ?>" alt=""></div></div>
				<?php endforeach?>
			</div>
		</div>
</section>
