<div class="ulockd-inner-home">
		<div class="container text-center">
			<div class="row">
				<div class="ulockd-inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase">Visi & Misi</h1>
					</div>
					<div class="col-md-12">
						<div class="ulockd-icd-layer">
							<ul class="list-inline ulockd-icd-sub-menu">
								<li><a href="#"> Beranda </a></li>
								<li><a href="#"> > </a></li>
								<li> <a href="#"> Visi & Misi </a> </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>

<section class="ulockd-about-two stellar">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-lg-offset-3 text-center">
					<div class="ulockd-testimonial-title hvr-float-shadow">
						<div class="ulockd-title-icon"><span class="flaticon-medical-kit"></span></div>
						<h2 class="text-uppercase">Visi & Misi</h2>

					</div>
				</div>
				<div class="col-md-6">
					<div class="ulockd-abouttwo-details">
						<?php echo $visi_misi->vismis_deskripsi;?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="ulockd-fector-about-video">
						<div class="ulockd-afv-thumb">
							<img class="img-responsive img-whp" src="<?php echo base_url().'assets/images/'.$visi_misi->vismis_gambar; ?>" alt="about1.png">
						</div>
					</div>
				</div>
			</div>

		</div>
</section>
