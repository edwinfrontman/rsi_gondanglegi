<?php foreach ($informasi as $event){
  $deskripsi = $event['infoum_deskripsi'];
	$cut=substr($deskripsi,0,150);
?>

<div class="col-xs-12 col-sm-6 col-md-4">
  <article class="ulockd-blog-post">
    <div class="ulockd-bp-thumb">
      <img class="img-responsive img-whp" src="<?php echo base_url().'assets/images/'.$event['infoum_gambar']; ?>" alt="1.jpg">
    </div>
      <div class="ulockd-bp-details text-left">
        <div class="ulockd-bp-title"><h4><?=$event['infoum_judul'];?></h4></div>
        <ul class="list-inline">
          <li class="ulockd-post-by"><a href="#"> <i class="fa fa-calendar text-thm1"> </i><?= longdate_indo($event['infoum_tanggal']);?></a></li>
        </ul>
        <div class="ulockd-bpost">
          <p><?php echo $cut;?> <a class="ulockd-bp-btn" href="<?php echo base_url('Informasi_umum/detail_informasi/'.$event['infoum_id']); ?>"> Baca Selengkapnya...</a></p>
        </div>
      </div>
  </article>
</div>
<?php } ?>
