<style>


</style>

<div class="ulockd-home-slider">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 ulockd-pmz">
					<div class="ulockd-main-slider">
						<?php foreach($tampil_slider as $slider):?>
						<div class="item">
							<div class="caption style1 text-center"  style="background: url(<?php echo base_url().'assets/images/'.$slider['slider_gambar']; ?>);-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;background-position: center center;background-repeat: no-repeat;">
								<div class="ulockd-slider-text1 wow fadeInUp" data-wow-duration="300ms" data-wow-delay=".3s"><?php echo $slider['slider_judul']?></div>
								<div class="ulockd-slider-text2 wow fadeInUp" data-wow-duration="600ms" data-wow-delay=".6s"><?php echo $slider['slider_subjudul']?></div>
								<div class="ulockd-slider-text3 wow fadeInUp" data-wow-duration="900ms" data-wow-delay=".9s">
									<p><?php echo $slider['slider_deskripsi']?></p>
								</div>
								<a href="#" class="btn btn-lg ulockd-btn-thm ulockd-home-btn wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="1.2s"><span> Get A Quote</span></a>
							</div>
						</div>
						<?php endforeach?>
					</div>
				</div>
			</div>
		</div>
	</div>



	<section class="ulockd-service-three">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-lg-offset-3 text-center">
					<div class="ulockd-testimonial-title hvr-float-shadow">
						<div class="ulockd-title-icon"><span class="flaticon-medical-kit"></span></div>
						<h2 class="text-uppercase">Foto Terbaru</h2>

					</div>
				</div>
			</div>
            <div class="row">
	            <div class="col-md-12">
	                <!-- End Masonry Filter -->

	                <!-- Masonry Grid -->
	                <div id="grid" class="masonry-gallery grid-4 clearfix">

		                <?php foreach($foto_terbaru as $foto):?>
		                <div class="isotope-item emergency dental">
		                    <div class="gallery-thumb">
			                    <img class="img-responsive img-whp" src="<?php echo base_url().'assets/images/'.$foto['foto_gambar']; ?>" alt="project">
			                    <div class="overlayer">
														<div class="lbox-caption">
															<div class="lbox-details">
																<h5><?php echo $foto['foto_nama']?></h5>
																<ul class="list-inline">
																	<li><a class="popup-img" href="<?php echo base_url().'assets/images/'.$foto['foto_gambar']; ?>" title="Gallery Photos"><span class="flaticon-add-square-button"></span></a></li>

																</ul>
															</div>
														</div>
			                    </div>
		                    </div>
		                </div>
										<?php endforeach?>


		                <!-- Masonry Item -->


		                <!-- Masonry = Masonry Item -->
	                </div>
	                <!-- Masonry Gallery Grid Item -->
	            </div>
            </div>
		</div>
	</section>
