<html dir="ltr" lang="en">

<!-- Mirrored from unlockdesizn.com/html/health-and-beauty/eHospital/demo/index-multipage.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Jan 2018 02:35:32 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="<?php echo $keyword ?>" />
<meta name="Description" content="<?php echo $description?>">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
<!-- Responsive stylesheet -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css">


<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<title><?php echo $title?></title>
<!-- Favicon -->
<link href="<?php echo base_url();?>assets/images/favicon.png" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="<?php echo base_url();?>assets/images/favicon.png" sizes="128x128" rel="shortcut icon" />

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
	<div class="preloader"></div>
	<div class="header-top">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="social-linked">
						<ul class="list-inline">
							<li><a href="https://www.facebook.com/<?php echo $facebook->setup_isi;?>"><i class="fa fa-facebook"></i></a></li>
							<li><a href="https://twitter.com/<?php echo $twitter->setup_isi ?>"><i class="fa fa-twitter"></i></a></li>
							<li><a href="https://www.instagram.com/<?php echo $instagram->setup_isi ?>"><i class="fa fa-instagram"></i></a></li>
							<li><a href="https://www.youtube.com/<?php echo $youtube->setup_isi ?>"><i class="fa fa-youtube-play"></i></a></li>
							<li><p class="ulockd-welcntxt">UGD<span class="text-thm1"><?php echo $telp->setup_isi ?></span></p></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="welcm-ht text-center">
						<p class="ulockd-welcntxt"><?php echo $sekilas->setup_isi ?></p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="welcm-ht text-right">
					<ul class="list-inline">

					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
  	<div class="header-middle">
  		<div class="container">
				<div class="row">
  				<div class="col-xs-12 col-sm-4 col-md-4">
					<div class="ulockd-contact-info text-right">
						<div class="ulockd-icon pull-right"><span class="flaticon-map-marker"></span></div>
						<div class="ulockd-info">
							<h3>Lokasi</h3>
							<p class="ulockd-cell">Lokasi mu disini</p>
						</div>
					</div>
  				</div>
  				<div class="col-xs-12 col-sm-4 col-md-4">
  					<div class="ulockd-welcm-hmddl text-center">
						<a href="#" class="ulockd-main-logo"><img src="<?php echo base_url().'assets/images/'.$header->setup_isi; ?>" style="width:291px;height:70px;" alt=""></a>
  					</div>
  				</div>
  				<div class="col-xs-12 col-sm-4 col-md-4">
					<div class="ulockd-ohour-info">
						<div class="ulockd-icon pull-left"><span class="flaticon-clock-1"></span></div>
						<div class="ulockd-info">
							<h3>Jam Buka</h3>
							<p class="ulockd-addrss">Sat-Thi 9:00-20:00</p>
						</div>
					</div>
  				</div>
  			</div>
  		</div>
  	</div>

	<!-- Header Styles -->
	<header class="header-nav">
		<div class="main-header-nav navbar-scrolltofixed">
			<div class="container">
			    <nav class="navbar navbar-default bootsnav menu-style1 light-blue">
					<!-- Start Top Search -->
			        <div class="top-search">
			            <div class="container">
			                <div class="input-group">
			                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
			                    <input type="text" class="form-control" placeholder="Search">
			                    <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
			                </div>
			            </div>
			        </div>
			        <!-- End Top Search -->

			        <div class="container ulockd-pdng0">
			            <div class="navbar-header">
			                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
			                    <i class="fa fa-bars"></i>
			                </button>
			            </div>
			            <!-- End Header Navigation -->

			            <!-- Collect the nav links, forms, and other content for toggling -->
			            <div class="collapse navbar-collapse" id="navbar-menu">
			                <ul class="nav navbar-nav navbar-left">
			                    <li>
			                        <a href="<?php echo base_url('Home') ?>">Beranda</a>
			                    </li>
			                    <li class="dropdown">
			                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tentang Kami</a>
			                        <ul class="dropdown-menu">
              										<li><a href="<?php echo base_url('Sejarah') ?>">Sejarah</a></li>
              										<li><a href="<?php echo base_url('VisiMisi') ?>">Visi Misi</a></li>
              										<li><a href="<?php echo base_url('Kontak') ?>">Kontak Kami</a></li>
			                        </ul>
			                    </li>
                          <li class="dropdown">
			                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Fasilitas Pelayanan</a>
			                        <ul class="dropdown-menu">
              										<li><a href="<?php echo base_url('Pelayanan') ?>">Pelayanan</a></li>
              										<li><a href="<?php echo base_url('Penunjang_Medis') ?>">Penunjang Medis</a></li>
              										<li><a href="<?php echo base_url('Fasilitas_Umum') ?>">Fasilitas Umum</a></li>
			                        </ul>
			                    </li>
                          <li>
                              <a href="<?php echo base_url('Info_Dokter') ?>">Info Dokter</a>
                          </li>
                          <li class="dropdown">
			                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Informasi</a>
			                        <ul class="dropdown-menu">
              										<li><a href="<?php echo base_url('Artikel_kesehatan') ?>">Artikel Kesehatan</a></li>
              										<li><a href="<?php echo base_url('Informasi_umum') ?>">Informasi Umum</a></li>
              										<li><a href="<?php echo base_url('Testimoni') ?>">Testimoni</a></li>
                                  <li class="dropdown">
                                    <a href="page-about3.html" class="dropdown-toggle" data-toggle="dropdown">Rekanan</a>
                                    <ul class="dropdown-menu">
																			<?php foreach($asuransi as $asuransie):?>
                												<li><a href="<?php echo base_url('Rekanan/detail/'.$asuransie['setup_kode']) ?>"><?php echo $asuransie['setup_isi']?></a></li>
																			<?php endforeach?>
	                                  </ul>

                                  </li>
			                        </ul>
			                    </li>

                          <li class="dropdown">
			                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Galeri</a>
			                        <ul class="dropdown-menu">
              										<li><a href="<?php echo base_url('Foto') ?>">Foto</a></li>
              										<li><a href="<?php echo base_url('Video') ?>">Video</a></li>
			                        </ul>
			                    </li>

                          <li>
                              <a href="<?php echo base_url('Indikator_mutu') ?>">Indikator Mutu</a>
                          </li>

			                </ul>
			            </div><!-- /.navbar-collapse -->
			        </div>

			        <!-- Start Side Menu -->
			        <div class="side">
			            <a href="#" class="close-side"><i class="fa fa-times"></i></a>
			            <div class="widget">
			                <h4 class="title">Custom Pages</h4>
			                <ul class="link">
			                    <li><a href="#">About</a></li>
			                    <li><a href="#">Services</a></li>
			                    <li><a href="#">Blog</a></li>
			                    <li><a href="#">Portfolio</a></li>
			                    <li><a href="#">Contact</a></li>
			                </ul>
			            </div>
			            <div class="widget">
			                <h4 class="title">Additional Links</h4>
			                <ul class="link">
			                    <li><a href="#">Retina Homepage</a></li>
			                    <li><a href="#">New Page Examples</a></li>
			                    <li><a href="#">Parallax Sections</a></li>
			                    <li><a href="#">Shortcode Central</a></li>
			                    <li><a href="#">Ultimate Font Collection</a></li>
			                </ul>
			            </div>
			        </div>
			        <!-- End Side Menu -->

			    </nav>
			</div>
		</div>
	</header>
