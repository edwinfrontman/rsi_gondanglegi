<div class="ulockd-inner-home">
		<div class="container text-center">
			<div class="row">
				<div class="ulockd-inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase">Foto</h1>
					</div>
					<div class="col-md-12">
						<div class="ulockd-icd-layer">
							<ul class="list-inline ulockd-icd-sub-menu">
								<li><a href="#"> Beranda </a></li>
								<li><a href="#"> > </a></li>
								<li> <a href="#"> Foto </a> </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
</div>
</div>
<section class="ulockd-service-three">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<div class="ulockd-testimonial-title hvr-float-shadow">
					<div class="ulockd-title-icon" style="right: 100px;"><span class="flaticon-medical-kit"></span></div>
					<h2 class="text-uppercase" style="margin-right: 99px;">Foto</h2>

				</div>
			</div>
		</div>
					<div class="row">
						<div class="col-md-12">
								<!-- End Masonry Filter -->

								<!-- Masonry Grid -->
								<div id="grid" class="masonry-gallery grid-4 clearfix">

									<?php foreach($foto as $fotoe):?>
									<div class="isotope-item emergency dental">
											<div class="gallery-thumb">
												<img class="img-responsive img-whp" src="<?php echo base_url().'assets/images/'.$fotoe['foto_gambar']; ?>" alt="project">
												<div class="overlayer">
													<div class="lbox-caption">
														<div class="lbox-details">
															<h5><?php echo $fotoe['foto_nama']?></h5>
															<ul class="list-inline">
																<li><a class="popup-img" href="<?php echo base_url().'assets/images/'.$fotoe['foto_gambar']; ?>" title="Gallery Photos"><span class="flaticon-add-square-button"></span></a></li>

															</ul>
														</div>
													</div>
												</div>
											</div>
									</div>
									<?php endforeach?>


									<!-- Masonry Item -->


									<!-- Masonry = Masonry Item -->
								</div>
								<!-- Masonry Gallery Grid Item -->
						</div>
					</div>
	</div>
</section>
