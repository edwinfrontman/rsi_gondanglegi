<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_artikelkesehatan extends CI_Model {

  public function get_artikel(){
    return $this->db->get('artikel_kesehatan');
  }

  public function get_all(){
		$this->db->order_by('arkes_id', 'DESC');
		return $this->db->get('artikel_kesehatan');
	}

	function get_all_artikel($limit, $offset){
		$this->db->from("artikel_kesehatan");
		$this->db->order_by("RAND()");
		$this->db->group_by('arkes_id');
		$this->db->limit($limit, $offset);
		return $this->db->get()->result_array();
	}

	function get_all_artikel_whe($limit, $offset, $jenis){
		$this->db->from("artikel_kesehatan");
		$this->db->where('artikel_kesehatan.arkes_id', $jenis);
		$this->db->order_by("RAND()");
		$this->db->group_by('artikel_kesehatan.arkes_judul');
		$this->db->limit($limit, $offset);
		return $this->db->get("artikel_kesehatan")->result();
	}

  public function artikel_kesehatan($id=null, $status=null){
		if($id==null){
			$this->db->limit(5);
		}else{
			if($status==null){
				$this->db->where('artikel_kesehatan.arkes_id', $id);
			}else{
				$this->db->limit(5);
			}
		}
		$this->db->from('artikel_kesehatan');
		$this->db->order_by("RAND()");
		return $this->db->get();
	}

  public function logo_footer(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','LOGO_FOOTER');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function title(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','TITLE');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function deskripsi(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','DESKRIPSI');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function artikel(){
    return $this->db->limit(2)->get('artikel_kesehatan');
  }

  public function kontak(){
    return $this->db->get('kontak');
  }

  public function get_limited($offset,$limit){
    $this->db->order_by('arkes_id', 'DESC');
    $this->db->limit($limit, $offset);
    return $this->db->get('artikel_kesehatan')->result_array();
  }

}
