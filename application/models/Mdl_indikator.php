<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_indikator extends CI_Model {

	public function get_indikator(){
	    return $this->db->get('indikator_mutu');
	}

	public function logo_footer(){
	    $this->db->where('setup_kode','1');
	    $this->db->where('setup_param','LOGO_FOOTER');
	    $query = $this->db->get('setup');
	    return $query->row();
	}

	public function title(){
	    $this->db->where('setup_kode','1');
	    $this->db->where('setup_param','TITLE');
	    $query = $this->db->get('setup');
	    return $query->row();
	}

	public function deskripsi(){
	    $this->db->where('setup_kode','1');
	    $this->db->where('setup_param','DESKRIPSI');
	    $query = $this->db->get('setup');
	    return $query->row();
	}

	public function artikel(){
	    return $this->db->limit(2)->get('artikel_kesehatan');
	}

	public function kontak(){
	    return $this->db->get('kontak');
	}

	public function get_limited($offset,$limit){
	    $this->db->order_by('arkes_id', 'DESC');
	    $this->db->limit($limit, $offset);
	    return $this->db->get('artikel_kesehatan')->result_array();
	}
}	