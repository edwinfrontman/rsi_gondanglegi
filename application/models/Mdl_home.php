<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_home extends CI_Model {

  public function tampil_slider(){
		return $this->db->order_by('slider_id','DESC')
						->get('slider');
	}

  public function get_foto(){
    return $this->db->limit(8)->get('foto');
  }

  public function logo_footer(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','LOGO_FOOTER');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function title(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','TITLE');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function deskripsi(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','DESKRIPSI');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function artikel(){
    return $this->db->limit(2)->get('artikel_kesehatan');
  }

  public function kontak(){
    return $this->db->get('kontak');
  }

  public function asuransi(){
    $this->db->from('setup');
    $this->db->where('setup_param','ASURANSI');
    return $this->db->get();
  }

  public function facebook(){
    $this->db->from('setup');
    $this->db->where('setup_param','FACEBOOK');
    return $this->db->get();
  }

  public function twitter(){
    $this->db->from('setup');
    $this->db->where('setup_param','TWITTER');
    return $this->db->get();
  }

  public function instagram(){
    $this->db->from('setup');
    $this->db->where('setup_param','INSTAGRAM');
    return $this->db->get();
  }

  public function youtube(){
    $this->db->from('setup');
    $this->db->where('setup_param','YOUTUBE');
    return $this->db->get();
  }

  public function sekilas(){
    $this->db->from('setup');
    $this->db->where('setup_param','SEKILAS');
    return $this->db->get();
  }

  public function telp(){
    $this->db->from('setup');
    $this->db->where('setup_param','TELP');
    return $this->db->get();
  }

  public function header(){
    $this->db->from('setup');
    $this->db->where('setup_param','LOGO_HEADER');
    return $this->db->get();
  }

}
