<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_dokter extends CI_Model {

  public function get_dokter(){
    return $this->db->get('dokter');
  }

  public function get_spesialis(){
    return $this->db->get('master_spesialis');
  }

  public function logo_footer(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','LOGO_FOOTER');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function title(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','TITLE');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function deskripsi(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','DESKRIPSI');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function artikel(){
    return $this->db->limit(2)->get('artikel_kesehatan');
  }

  public function kontak(){
    return $this->db->get('kontak');
  }

  public function getDokter($id) {
		$this->db->from('dokter');
		$this->db->where('spesialis_id', $id);
		$query = $this->db->get();
		return $query->result();
	}

}
