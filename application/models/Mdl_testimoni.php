<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_testimoni extends CI_Model {

  public function get_testimoni(){
    return $this->db->get('testimoni');
  }

  public function get_all(){
		$this->db->order_by('testimoni_id', 'DESC');
    $this->db->group_by('testimoni_nama');
		return $this->db->get('testimoni');
	}

	function get_all_testimoni($limit, $offset){
		$this->db->from("testimoni");
		$this->db->order_by("RAND()");
    $this->db->group_by('testimoni_nama');
		$this->db->limit($limit, $offset);
		return $this->db->get()->result_array();
	}

	function get_all_testimoni_whe($limit, $offset, $jenis){
		$this->db->from("testimoni");
		$this->db->where('testimoni.testimoni_id', $jenis);
		$this->db->order_by("RAND()");
    $this->db->group_by('testimoni_nama');
		$this->db->limit($limit, $offset);
		return $this->db->get("testimoni")->result();
	}

  public function logo_footer(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','LOGO_FOOTER');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function title(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','TITLE');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function deskripsi(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','DESKRIPSI');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function artikel(){
    return $this->db->limit(2)->get('artikel_kesehatan');
  }

  public function kontak(){
    return $this->db->get('kontak');
  }

  public function get_limited($offset,$limit){
		$this->db->order_by('testimoni_id', 'DESC');
		$this->db->limit($limit, $offset);
		return $this->db->get('testimoni')->result_array();
	}

}
