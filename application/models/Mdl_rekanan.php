<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_rekanan extends CI_Model {

  public function get_rekanan($id=null, $status=null){
    if($id==null){
      $this->db->limit(5);
    }else{
      if($status==null){
        $this->db->where('rekanan.rekanan_kode', $id);
      }else{
        $this->db->limit(5);
      }
    }
    $this->db->from('rekanan');
    $this->db->order_by("RAND()");
    return $this->db->get();
  }

  public function logo_footer(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','LOGO_FOOTER');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function title(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','TITLE');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function deskripsi(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','DESKRIPSI');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function artikel(){
    return $this->db->limit(2)->get('artikel_kesehatan');
  }

  public function kontak(){
    return $this->db->get('kontak');
  }

  public function asuransi(){
    $this->db->from('setup');
    $this->db->where('setup_param','ASURANSI');
    return $this->db->get();
  }

}
