<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_informasiumum extends CI_Model {

  public function get_informasi(){
    return $this->db->get('informasi_umum');
  }

  public function get_all(){
		$this->db->order_by('infoum_id', 'DESC');
		return $this->db->get('informasi_umum');
	}

	function get_all_informasi($limit, $offset){
		$this->db->from("informasi_umum");
		$this->db->order_by("RAND()");
		$this->db->group_by('infoum_id');
		$this->db->limit($limit, $offset);
		return $this->db->get()->result_array();
	}

	function get_all_informasi_whe($limit, $offset, $jenis){
		$this->db->from("informasi_umum");
		$this->db->where('informasi_umum.infoum_id', $jenis);
		$this->db->order_by("RAND()");
		$this->db->group_by('informasi_umum.infoum_judul');
		$this->db->limit($limit, $offset);
		return $this->db->get("informasi_umum")->result();
	}

  public function logo_footer(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','LOGO_FOOTER');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function title(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','TITLE');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function deskripsi(){
    $this->db->where('setup_kode','1');
    $this->db->where('setup_param','DESKRIPSI');
    $query = $this->db->get('setup');
    return $query->row();
  }

  public function artikel(){
    return $this->db->limit(2)->get('artikel_kesehatan');
  }

  public function kontak(){
    return $this->db->get('kontak');
  }

  public function informasi_umum($id=null, $status=null){
		if($id==null){
			$this->db->limit(5);
		}else{
			if($status==null){
				$this->db->where('informasi_umum.infoum_id', $id);
			}else{
				$this->db->limit(5);
			}
		}
		$this->db->from('informasi_umum');
		$this->db->order_by("RAND()");
		return $this->db->get();
	}

  public function get_limited($offset,$limit){
    $this->db->order_by('infoum_id', 'DESC');
    $this->db->limit($limit, $offset);
    return $this->db->get('informasi_umum')->result_array();
  }

}
