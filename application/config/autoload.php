<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array('form_validation','user_agent','upload', 'database', 'session', 'Auth');

$autoload['drivers'] = array();

// $autoload['helper'] = array('url', 'form', 'cookie', 'file');
$autoload['helper'] = array('url','form','security','file','tgl_indo');

$autoload['config'] = array();

$autoload['language'] = array();

$autoload['model'] = array();
