<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekanan extends CI_Controller {

  public function __construct()
	{
		parent::__construct();
    	$this->load->helper('form');
    	$this->load->library('form_validation');
    	$this->load->model('Mdl_rekanan');
      $this->load->model('Mdl_home');
      date_default_timezone_set("Asia/Jakarta");
	}

 //  public function asuransi()
	// {
 //    $this->load->view('header');

 //    $this->load->view('asuransi');

	// 	$this->load->view('footer');
 //  }

 //  public function non_asuransi()
 //  {
 //    $this->load->view('header');

 //    $this->load->view('non_asuransi');

 //    $this->load->view('footer');
 //  }

  public function detail($id){
    $rekanan_meta = $this->Mdl_rekanan->get_rekanan($id)->row();
    $data_header['title'] = $rekanan_meta->rekanan_title_meta;
    $data_header['description'] = $rekanan_meta->rekanan_deskripsi_meta;
    $data_header['keyword'] = $rekanan_meta->rekanan_keyword_meta;
    $data_header['asuransi'] = $this->Mdl_home->asuransi()->result_array();
    $data_header['facebook'] = $this->Mdl_home->facebook()->row();
    $data_header['twitter'] = $this->Mdl_home->twitter()->row();
    $data_header['instagram'] = $this->Mdl_home->instagram()->row();
    $data_header['youtube'] = $this->Mdl_home->youtube()->row();
    $data_header['sekilas'] = $this->Mdl_home->sekilas()->row();
    $data_header['telp'] = $this->Mdl_home->telp()->row();
    $data_header['header'] = $this->Mdl_home->header()->row();
    $this->load->view('header', $data_header);

    $data['asuransi']= $this->Mdl_rekanan->get_rekanan($id)->result_array();
   // print_r($this->db->last_query());
    $this->load->view('asuransi', $data);

    $data_footer['logo'] = $this->Mdl_rekanan->logo_footer();
    $data_footer['title'] = $this->Mdl_rekanan->title();
    $data_footer['deskripsi'] = $this->Mdl_rekanan->deskripsi();
    $data_footer['artikel'] = $this->Mdl_rekanan->artikel()->result_array();
    $data_footer['kontak'] = $this->Mdl_rekanan->kontak()->row();
    $this->load->view('footer', $data_footer);
  }

}
