<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penunjang_Medis extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->model('Mdl_penunjang');
    $this->load->model('Mdl_home');
    date_default_timezone_set("Asia/Jakarta");
  }

  public function index()
  {
    $penunjang_meta = $this->Mdl_penunjang->get_penunjang()->row();
	  $data_header['title'] = $penunjang_meta->penunjang_title_meta;
		$data_header['description'] = $penunjang_meta->penunjang_deksripsi_meta;
		$data_header['keyword'] = $penunjang_meta->penunjang_keyword_meta;
    $data_header['asuransi'] = $this->Mdl_home->asuransi()->result_array();
    $data_header['facebook'] = $this->Mdl_home->facebook()->row();
    $data_header['twitter'] = $this->Mdl_home->twitter()->row();
    $data_header['instagram'] = $this->Mdl_home->instagram()->row();
    $data_header['youtube'] = $this->Mdl_home->youtube()->row();
    $data_header['sekilas'] = $this->Mdl_home->sekilas()->row();
    $data_header['telp'] = $this->Mdl_home->telp()->row();
    $data_header['header'] = $this->Mdl_home->header()->row();
    $this->load->view('header', $data_header);

    $data['penunjang'] = $this->Mdl_penunjang->get_penunjang()->row();
    $this->load->view('penunjang_medis', $data);

    $data_footer['logo'] = $this->Mdl_penunjang->logo_footer();
    $data_footer['title'] = $this->Mdl_penunjang->title();
    $data_footer['deskripsi'] = $this->Mdl_penunjang->deskripsi();
    $data_footer['artikel'] = $this->Mdl_penunjang->artikel()->result_array();
    $data_footer['kontak'] = $this->Mdl_penunjang->kontak()->row();
    $this->load->view('footer', $data_footer);
  }
}
