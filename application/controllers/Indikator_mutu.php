<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indikator_mutu extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->model('Mdl_indikator');
    $this->load->model('Mdl_home');
    date_default_timezone_set("Asia/Jakarta");
  }

  public function index()
  {
    $data_header['title'] = 'RSI Gondanglegi';
    $data_header['description'] = 'Rsi Gondanglegi Adalah Salah Satu Rumah Sakit Islam Di Malang';
    $data_header['keyword'] = 'Rsi Gondanglegi Malang';
    $data_header['asuransi'] = $this->Mdl_home->asuransi()->result_array();
    $data_header['facebook'] = $this->Mdl_home->facebook()->row();
    $data_header['twitter'] = $this->Mdl_home->twitter()->row();
    $data_header['instagram'] = $this->Mdl_home->instagram()->row();
    $data_header['youtube'] = $this->Mdl_home->youtube()->row();
    $data_header['sekilas'] = $this->Mdl_home->sekilas()->row();
    $data_header['telp'] = $this->Mdl_home->telp()->row();
    $data_header['header'] = $this->Mdl_home->header()->row();
    $this->load->view('header', $data_header);

    $data['indikator']= $this->Mdl_indikator->get_indikator()->result();
    $this->load->view('indikator_mutu', $data);

    $data_footer['logo'] = $this->Mdl_indikator->logo_footer();
    $data_footer['title'] = $this->Mdl_indikator->title();
    $data_footer['deskripsi'] = $this->Mdl_indikator->deskripsi();
    $data_footer['artikel'] = $this->Mdl_indikator->artikel()->result_array();
    $data_footer['kontak'] = $this->Mdl_indikator->kontak()->row();
    $this->load->view('footer', $data_footer);
  }


}
