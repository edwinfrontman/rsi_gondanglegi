<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info_Dokter extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->model('Mdl_dokter');
    $this->load->model('Mdl_home');
    date_default_timezone_set("Asia/Jakarta");
  }

  public function index()
  {
    $dokter_meta = $this->Mdl_dokter->get_dokter()->row();
	  $data_header['title'] = $dokter_meta->dokter_title_meta;
		$data_header['description'] = $dokter_meta->dokter_deskripsi_meta;
		$data_header['keyword'] = $dokter_meta->dokter_keyword_meta;
    $data_header['asuransi'] = $this->Mdl_home->asuransi()->result_array();
    $data_header['asuransi'] = $this->Mdl_home->asuransi()->result_array();
    $data_header['facebook'] = $this->Mdl_home->facebook()->row();
    $data_header['twitter'] = $this->Mdl_home->twitter()->row();
    $data_header['instagram'] = $this->Mdl_home->instagram()->row();
    $data_header['youtube'] = $this->Mdl_home->youtube()->row();
    $data_header['sekilas'] = $this->Mdl_home->sekilas()->row();
    $data_header['telp'] = $this->Mdl_home->telp()->row();
    $data_header['header'] = $this->Mdl_home->header()->row();
    $this->load->view('header', $data_header);

    $data['spesialis'] = $this->Mdl_dokter->get_spesialis()->result_array();
    $this->load->view('info_dokter', $data);

    $data_footer['logo'] = $this->Mdl_dokter->logo_footer();
    $data_footer['title'] = $this->Mdl_dokter->title();
    $data_footer['deskripsi'] = $this->Mdl_dokter->deskripsi();
    $data_footer['artikel'] = $this->Mdl_dokter->artikel()->result_array();
    $data_footer['kontak'] = $this->Mdl_dokter->kontak()->row();
    $this->load->view('footer', $data_footer);
  }

  function create_load(){
		$this->load->view('detail_dokter');
	}

  public function getDokter(){
    	$kode = $this->uri->segment(3);
       // $jurusan = $this->uri->segment(4);
       // $prodi   = $this->uri->segment(5);

        $data = $this->Mdl_dokter->getDokter($kode);
            echo "<option value=''>- Pilihan Nama Dokter -</option>";
        foreach ($data as $hasil) {
            echo '<option value="'.$hasil->spesialis_id.'/'.$hasil->dokter_id.'">'.$hasil->dokter_nama.'</option>';
        }
    }

}
