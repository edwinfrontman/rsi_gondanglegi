<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VisiMisi extends CI_Controller {

  public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('Mdl_visi');
    $this->load->model('Mdl_home');
    date_default_timezone_set("Asia/Jakarta");
	}

  public function index()
	{
    $visi_meta = $this->Mdl_visi->get_visi()->row();
	  $data_header['title'] = $visi_meta->vismis_title_meta;
		$data_header['description'] = $visi_meta->vismis_deskripsi_meta;
		$data_header['keyword'] = $visi_meta->vismis_keyword_meta;
    $data_header['asuransi'] = $this->Mdl_home->asuransi()->result_array();
    $data_header['facebook'] = $this->Mdl_home->facebook()->row();
    $data_header['twitter'] = $this->Mdl_home->twitter()->row();
    $data_header['instagram'] = $this->Mdl_home->instagram()->row();
    $data_header['youtube'] = $this->Mdl_home->youtube()->row();
    $data_header['sekilas'] = $this->Mdl_home->sekilas()->row();
    $data_header['telp'] = $this->Mdl_home->telp()->row();
    $data_header['header'] = $this->Mdl_home->header()->row();
    $this->load->view('header', $data_header);

    $data['visi_misi'] = $this->Mdl_visi->get_visi()->row();
    $this->load->view('visi_misi', $data);

    $data_footer['logo'] = $this->Mdl_visi->logo_footer();
    $data_footer['title'] = $this->Mdl_visi->title();
    $data_footer['deskripsi'] = $this->Mdl_visi->deskripsi();
    $data_footer['artikel'] = $this->Mdl_visi->artikel()->result_array();
    $data_footer['kontak'] = $this->Mdl_visi->kontak()->row();
		$this->load->view('footer', $data_footer);
  }

}
