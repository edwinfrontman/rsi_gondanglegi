<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Informasi_umum extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->model('Mdl_informasiumum');
    $this->load->model('Mdl_home');
    date_default_timezone_set("Asia/Jakarta");
  }

  public function index()
  {
    $informasi_meta = $this->Mdl_informasiumum->get_informasi()->row();
    $data_header['title'] = $informasi_meta->infoum_title_meta;
    $data_header['description'] = $informasi_meta->infoum_keyword_meta;
    $data_header['keyword'] = $informasi_meta->infoum_deskripsi_meta;
    $data_header['asuransi'] = $this->Mdl_home->asuransi()->result_array();
    $data_header['facebook'] = $this->Mdl_home->facebook()->row();
    $data_header['twitter'] = $this->Mdl_home->twitter()->row();
    $data_header['instagram'] = $this->Mdl_home->instagram()->row();
    $data_header['youtube'] = $this->Mdl_home->youtube()->row();
    $data_header['sekilas'] = $this->Mdl_home->sekilas()->row();
    $data_header['telp'] = $this->Mdl_home->telp()->row();
    $data_header['header'] = $this->Mdl_home->header()->row();
    $this->load->view('header', $data_header);

    $data['informasi'] = $this->Mdl_informasiumum->get_limited(0,5);
    $total_data = $this->Mdl_informasiumum->get_all()->num_rows();
		$data['total_data'] = ceil($total_data/3);
		$data['jenis'] = null;
    $this->load->view('informasi_umum', $data);

    $data_footer['logo'] = $this->Mdl_informasiumum->logo_footer();
    $data_footer['title'] = $this->Mdl_informasiumum->title();
    $data_footer['deskripsi'] = $this->Mdl_informasiumum->deskripsi();
    $data_footer['artikel'] = $this->Mdl_informasiumum->artikel()->result_array();
    $data_footer['kontak'] = $this->Mdl_informasiumum->kontak()->row();
    $this->load->view('footer', $data_footer);
  }

  // public function load_informasi($jenis=null){
	// 	$offset = ceil($this->input->post('offset')*3);
	// 	if(empty($jenis)){
	// 		$data['informasi'] = $this->Mdl_informasiumum->get_all_informasi(3,0);
	// 	}else{
	// 		$data['informasi'] = $this->Mdl_informasiumum->get_all_informasi_whe(3,0, $jenis);
	// 	}
	// 	$this->load->view('load_informasi', $data);
	// }

  public function load_informasi(){
    //$this->load->model('M_event');
    $offset = ceil($this->input->post('offset')*3);
    $data['informasi'] = $this->Mdl_informasiumum->get_limited($offset,3);
    echo $this->load->view('load_informasi',$data,true);
  }

  public function detail_informasi($id)
  {
    $informasi_meta = $this->Mdl_informasiumum->get_informasi()->row();
    $data_header['title'] = $informasi_meta->infoum_title_meta;
    $data_header['description'] = $informasi_meta->infoum_keyword_meta;
    $data_header['keyword'] = $informasi_meta->infoum_deskripsi_meta;
    $data_header['asuransi'] = $this->Mdl_home->asuransi()->result_array();
    $data_header['facebook'] = $this->Mdl_home->facebook()->row();
    $data_header['twitter'] = $this->Mdl_home->twitter()->row();
    $data_header['instagram'] = $this->Mdl_home->instagram()->row();
    $data_header['youtube'] = $this->Mdl_home->youtube()->row();
    $data_header['sekilas'] = $this->Mdl_home->sekilas()->row();
    $data_header['telp'] = $this->Mdl_home->telp()->row();
    $data_header['header'] = $this->Mdl_home->header()->row();
    $this->load->view('header', $data_header);

    $data['detail'] = $this->Mdl_informasiumum->informasi_umum($id)->result_array();
		$data['detail_informasi'] = $this->Mdl_informasiumum->informasi_umum($id)->row();
    $this->load->view('detail_informasi', $data);

    $data_footer['logo'] = $this->Mdl_informasiumum->logo_footer();
    $data_footer['title'] = $this->Mdl_informasiumum->title();
    $data_footer['deskripsi'] = $this->Mdl_informasiumum->deskripsi();
    $data_footer['artikel'] = $this->Mdl_informasiumum->artikel()->result_array();
    $data_footer['kontak'] = $this->Mdl_informasiumum->kontak()->row();
    $this->load->view('footer', $data_footer);
  }

}
