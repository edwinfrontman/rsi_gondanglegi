<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelayanan extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->model('Mdl_pelayanan');
    $this->load->model('Mdl_home');
    date_default_timezone_set("Asia/Jakarta");
  }

  public function index()
  {
    $pelayanan_meta = $this->Mdl_pelayanan->get_pelayanan()->row();
	  $data_header['title'] = $pelayanan_meta->pelayanan_title_meta;
		$data_header['description'] = $pelayanan_meta->pelayanan_deksripsi_meta;
		$data_header['keyword'] = $pelayanan_meta->pelayanan_keyword_meta;
    $data_header['asuransi'] = $this->Mdl_home->asuransi()->result_array();
    $data_header['facebook'] = $this->Mdl_home->facebook()->row();
    $data_header['twitter'] = $this->Mdl_home->twitter()->row();
    $data_header['instagram'] = $this->Mdl_home->instagram()->row();
    $data_header['youtube'] = $this->Mdl_home->youtube()->row();
    $data_header['sekilas'] = $this->Mdl_home->sekilas()->row();
    $data_header['telp'] = $this->Mdl_home->telp()->row();
    $data_header['header'] = $this->Mdl_home->header()->row();
    $this->load->view('header', $data_header);

    $data['pelayanan'] = $this->Mdl_pelayanan->get_pelayanan()->row();
    $this->load->view('pelayanan', $data);

    $data_footer['logo'] = $this->Mdl_pelayanan->logo_footer();
    $data_footer['title'] = $this->Mdl_pelayanan->title();
    $data_footer['deskripsi'] = $this->Mdl_pelayanan->deskripsi();
    $data_footer['artikel'] = $this->Mdl_pelayanan->artikel()->result_array();
    $data_footer['kontak'] = $this->Mdl_pelayanan->kontak()->row();
    $this->load->view('footer', $data_footer);
  }
}
