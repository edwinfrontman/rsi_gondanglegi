<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Video extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->model('Mdl_video');
    $this->load->model('Mdl_home');
    date_default_timezone_set("Asia/Jakarta");
  }

  public function index()
  {
    $video_meta = $this->Mdl_video->get_video()->row();
	  $data_header['title'] = $video_meta->video_title_meta;
		$data_header['description'] = $video_meta->video_deskripsi_meta;
		$data_header['keyword'] = $video_meta->video_keyword_meta;
    $data_header['asuransi'] = $this->Mdl_home->asuransi()->result_array();
    $data_header['facebook'] = $this->Mdl_home->facebook()->row();
    $data_header['twitter'] = $this->Mdl_home->twitter()->row();
    $data_header['instagram'] = $this->Mdl_home->instagram()->row();
    $data_header['youtube'] = $this->Mdl_home->youtube()->row();
    $data_header['sekilas'] = $this->Mdl_home->sekilas()->row();
    $data_header['telp'] = $this->Mdl_home->telp()->row();
    $data_header['header'] = $this->Mdl_home->header()->row();
    $this->load->view('header', $data_header);

    $data['video'] = $this->Mdl_video->get_video()->result_array();
    $this->load->view('video', $data);

    $data_footer['logo'] = $this->Mdl_video->logo_footer();
    $data_footer['title'] = $this->Mdl_video->title();
    $data_footer['deskripsi'] = $this->Mdl_video->deskripsi();
    $data_footer['artikel'] = $this->Mdl_video->artikel()->result_array();
    $data_footer['kontak'] = $this->Mdl_video->kontak()->row();
    $this->load->view('footer', $data_footer);
  }


}
