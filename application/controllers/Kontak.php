<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak extends CI_Controller {

  public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('Mdl_kontak');
    $this->load->model('Mdl_home');
    date_default_timezone_set("Asia/Jakarta");
	}

  public function index()
	{
    $kontak_meta = $this->Mdl_kontak->get_kontak()->row();
	$data_header['title'] = $kontak_meta->kontak_title_meta;
	$data_header['description'] = $kontak_meta->kontak_deskripsi_meta;
	$data_header['keyword'] = $kontak_meta->kontak_keyword_meta;
    $data_header['asuransi'] = $this->Mdl_home->asuransi()->result_array();
    $data_header['facebook'] = $this->Mdl_home->facebook()->row();
    $data_header['twitter'] = $this->Mdl_home->twitter()->row();
    $data_header['instagram'] = $this->Mdl_home->instagram()->row();
    $data_header['youtube'] = $this->Mdl_home->youtube()->row();
    $data_header['sekilas'] = $this->Mdl_home->sekilas()->row();
    $data_header['telp'] = $this->Mdl_home->telp()->row();
    $data_header['header'] = $this->Mdl_home->header()->row();
    $this->load->view('header', $data_header);

    $data['kontak'] = $this->Mdl_kontak->get_kontak()->row();
    $this->load->view('kontak', $data);

    $data_footer['logo'] = $this->Mdl_kontak->logo_footer();
    $data_footer['title'] = $this->Mdl_kontak->title();
    $data_footer['deskripsi'] = $this->Mdl_kontak->deskripsi();
    $data_footer['artikel'] = $this->Mdl_kontak->artikel()->result_array();
    $data_footer['kontak'] = $this->Mdl_kontak->kontak()->row();
		$this->load->view('footer', $data_footer);
  }

}
