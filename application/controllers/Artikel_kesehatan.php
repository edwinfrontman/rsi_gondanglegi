<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel_kesehatan extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->model('Mdl_artikelkesehatan');
    $this->load->model('Mdl_home');
    date_default_timezone_set("Asia/Jakarta");
  }

  public function index()
  {
    $artikel_meta = $this->Mdl_artikelkesehatan->get_artikel()->row();
    $data_header['title'] = $artikel_meta->arkes_title_meta;
    $data_header['description'] = $artikel_meta->arkes_keyword_meta;
    $data_header['keyword'] = $artikel_meta->arkes_deskripsi_meta;
    $data_header['asuransi'] = $this->Mdl_home->asuransi()->result_array();
    $data_header['facebook'] = $this->Mdl_home->facebook()->row();
    $data_header['twitter'] = $this->Mdl_home->twitter()->row();
    $data_header['instagram'] = $this->Mdl_home->instagram()->row();
    $data_header['youtube'] = $this->Mdl_home->youtube()->row();
    $data_header['sekilas'] = $this->Mdl_home->sekilas()->row();
    $data_header['telp'] = $this->Mdl_home->telp()->row();
    $data_header['header'] = $this->Mdl_home->header()->row();
    $this->load->view('header', $data_header);

    $data['artikel'] = $this->Mdl_artikelkesehatan->get_limited(0,5);
    $total_data = $this->Mdl_artikelkesehatan->get_all()->num_rows();
		$data['total_data'] = ceil($total_data/3);
		$data['jenis'] = null;
    $this->load->view('artikel_kesehatan', $data);

    $data_footer['logo'] = $this->Mdl_artikelkesehatan->logo_footer();
    $data_footer['title'] = $this->Mdl_artikelkesehatan->title();
    $data_footer['deskripsi'] = $this->Mdl_artikelkesehatan->deskripsi();
    $data_footer['artikel'] = $this->Mdl_artikelkesehatan->artikel()->result_array();
    $data_footer['kontak'] = $this->Mdl_artikelkesehatan->kontak()->row();
    $this->load->view('footer', $data_footer);
  }

  // public function load_artikel($jenis=null){
	// 	$offset = ceil($this->input->post('offset')*3);
	// 	if(empty($jenis)){
	// 		$data['artikel'] = $this->Mdl_artikelkesehatan->get_all_artikel(3,0);
	// 	}else{
	// 		$data['artikel'] = $this->Mdl_artikelkesehatan->get_all_artikel_whe(3,0, $jenis);
	// 	}
	// 	$this->load->view('load_artikel', $data);
	// }

  public function load_artikel(){
    //$this->load->model('M_event');
    $offset = ceil($this->input->post('offset')*3);
    $data['artikel'] = $this->Mdl_artikelkesehatan->get_limited($offset,3);
    echo $this->load->view('load_artikel',$data,true);
  }

  public function detail_artikel($id)
  {
    $artikel_meta = $this->Mdl_artikelkesehatan->get_artikel()->row();
    $data_header['title'] = $artikel_meta->arkes_title_meta;
    $data_header['description'] = $artikel_meta->arkes_keyword_meta;
    $data_header['keyword'] = $artikel_meta->arkes_deskripsi_meta;
    $data_header['asuransi'] = $this->Mdl_home->asuransi()->result_array();
    $data_header['facebook'] = $this->Mdl_home->facebook()->row();
    $data_header['twitter'] = $this->Mdl_home->twitter()->row();
    $data_header['instagram'] = $this->Mdl_home->instagram()->row();
    $data_header['youtube'] = $this->Mdl_home->youtube()->row();
    $data_header['sekilas'] = $this->Mdl_home->sekilas()->row();
    $data_header['telp'] = $this->Mdl_home->telp()->row();
    $data_header['header'] = $this->Mdl_home->header()->row();
    $this->load->view('header', $data_header);

    $data['detail'] = $this->Mdl_artikelkesehatan->artikel_kesehatan($id)->result_array();
		$data['detail_artikel'] = $this->Mdl_artikelkesehatan->artikel_kesehatan($id)->row();
    $this->load->view('detail_artikel', $data);

    $data_footer['logo'] = $this->Mdl_artikelkesehatan->logo_footer();
    $data_footer['title'] = $this->Mdl_artikelkesehatan->title();
    $data_footer['deskripsi'] = $this->Mdl_artikelkesehatan->deskripsi();
    $data_footer['artikel'] = $this->Mdl_artikelkesehatan->artikel()->result_array();
    $data_footer['kontak'] = $this->Mdl_artikelkesehatan->kontak()->row();
    $this->load->view('footer', $data_footer);
  }

}
