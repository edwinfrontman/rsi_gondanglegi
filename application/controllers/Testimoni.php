<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->model('Mdl_testimoni');
    $this->load->model('Mdl_home');
    date_default_timezone_set("Asia/Jakarta");
  }

  public function index()
  {
    $testimoni_meta = $this->Mdl_testimoni->get_testimoni()->row();
	  $data_header['title'] = $testimoni_meta->testimoni_title_meta;
		$data_header['description'] = $testimoni_meta->testimoni_deskripsi_meta;
		$data_header['keyword'] = $testimoni_meta->testimoni_keyword_meta;
    $data_header['asuransi'] = $this->Mdl_home->asuransi()->result_array();
    $data_header['facebook'] = $this->Mdl_home->facebook()->row();
    $data_header['twitter'] = $this->Mdl_home->twitter()->row();
    $data_header['instagram'] = $this->Mdl_home->instagram()->row();
    $data_header['youtube'] = $this->Mdl_home->youtube()->row();
    $data_header['sekilas'] = $this->Mdl_home->sekilas()->row();
    $data_header['telp'] = $this->Mdl_home->telp()->row();
    $data_header['header'] = $this->Mdl_home->header()->row();
    $this->load->view('header', $data_header);

    $data['testimoni'] = $this->Mdl_testimoni->get_limited(0,5);
    $total_data = $this->Mdl_testimoni->get_all()->num_rows();
		$data['total_data'] = ceil($total_data/3);
		$data['jenis'] = null;
    $this->load->view('testimoni', $data);

    $data_footer['logo'] = $this->Mdl_testimoni->logo_footer();
    $data_footer['title'] = $this->Mdl_testimoni->title();
    $data_footer['deskripsi'] = $this->Mdl_testimoni->deskripsi();
    $data_footer['artikel'] = $this->Mdl_testimoni->artikel()->result_array();
    $data_footer['kontak'] = $this->Mdl_testimoni->kontak()->row();
    $this->load->view('footer', $data_footer);
  }

  // public function load_testimoni($jenis=null){
	// 	$offset = ceil($this->input->post('offset')*3);
	// 	if(empty($jenis)){
	// 		$data['testimoni'] = $this->Mdl_testimoni->get_all_testimoni(3,0);
	// 	}else{
	// 		$data['testimoni'] = $this->Mdl_testimoni->get_all_testimoni_whe(3,0, $jenis);
	// 	}
	// 	$this->load->view('load_testimoni', $data);
	// }

  public function load_testimoni(){
    //$this->load->model('M_event');
    $offset = ceil($this->input->post('offset')*3);
    $data['testimoni'] = $this->Mdl_testimoni->get_limited($offset,3);
    echo $this->load->view('load_testimoni',$data,true);
  }

}
