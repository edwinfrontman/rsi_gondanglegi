<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Foto extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->model('Mdl_foto');
    $this->load->model('Mdl_home');
    date_default_timezone_set("Asia/Jakarta");
  }

  public function index()
  {
    $foto_meta = $this->Mdl_foto->get_foto()->row();
	  $data_header['title'] = $foto_meta->foto_title_meta;
		$data_header['description'] = $foto_meta->foto_deskripsi_meta;
		$data_header['keyword'] = $foto_meta->foto_keyword_meta;
    $data_header['asuransi'] = $this->Mdl_home->asuransi()->result_array();
    $data_header['facebook'] = $this->Mdl_home->facebook()->row();
    $data_header['twitter'] = $this->Mdl_home->twitter()->row();
    $data_header['instagram'] = $this->Mdl_home->instagram()->row();
    $data_header['youtube'] = $this->Mdl_home->youtube()->row();
    $data_header['sekilas'] = $this->Mdl_home->sekilas()->row();
    $data_header['telp'] = $this->Mdl_home->telp()->row();
    $data_header['header'] = $this->Mdl_home->header()->row();
    $this->load->view('header', $data_header);

    $data['foto'] = $this->Mdl_foto->get_foto()->result_array();
    $this->load->view('foto', $data);

    $data_footer['logo'] = $this->Mdl_foto->logo_footer();
    $data_footer['title'] = $this->Mdl_foto->title();
    $data_footer['deskripsi'] = $this->Mdl_foto->deskripsi();
    $data_footer['artikel'] = $this->Mdl_foto->artikel()->result_array();
    $data_footer['kontak'] = $this->Mdl_foto->kontak()->row();
    $this->load->view('footer', $data_footer);
  }

}
